import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import * as $ from 'jquery';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NavbarComponent } from '../navbar/navbar.component';
import { TradesComponent } from '../trades/trades.component';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
//import { C } from '@angular/core/src/render3';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: any;
  password: any;
  userId: any;
  error: boolean;
  accessToken: any;
  refreshToken: any;
  expiresIn: any;
  logoutReason: any;
  loader: boolean;
  currentRoute;
  otpBlock: boolean = false;
  otp: any;
  getlang:any;
  distance:any;
  private loginapi:Subscription;
  private loginotpApi:Subscription;
  private setlogdataApi: Subscription;
  private sendsmsapi: Subscription;
  private otplogin:any;
  constructor(
    private http: HttpClient,
    public data: CoreDataService,
    private cookie: CookieService,
    private route: Router,
    private nav: NavbarComponent,
    private trade: TradesComponent, public translate: TranslateService, private modalService: NgbModal) {
    translate.addLangs(['Turkish', 'English']);
    this.getlang = localStorage.getItem("selectedlang");
      if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang('Turkish');
      const browserLang = translate.getBrowserLang();
      translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    else {
      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();
      translate.use(browserLang.match(/Turkish|English/) ? browserLang : this.getlang);
    }
  }

  ngOnInit() {
    this.error = false;
    this.loader = false;
    this.logoutReason = this.nav.reason;
    this.route.events.subscribe(val => {
      this.currentRoute = val;
      if (this.currentRoute.url != '/dashboard') {
      }
    });
  }

  Selectedlangulage(lang) {
    localStorage.setItem('selectedlang', lang);
  }

  basic() {
  }

  loginData(isValid: boolean,content) {

    if (isValid && this.email != "" && this.password != "") {
      this.loader = true;
      this.logoutReason = '';
      var loginObj = {};
      loginObj['email'] = this.email;
      loginObj['password'] = this.password;
      var jsonString = JSON.stringify(loginObj);
      this.loginapi=this.http.post<any>(this.data.WEBSERVICE + '/user/LoginWithUsernamePassword', jsonString, { headers: { 'content-Type': 'application/json' } })
        .subscribe(data => {
          if (data.error.error_data == '1') {
            this.error = true;
            this.loader = false;
          }
          if (data.error.error_data == '0') {
            this.error = false;
          var UserID=data.userResult.userId;
          localStorage.setItem("UserID", UserID);
            if (data.userResult.twoFactorAuth == 0) {
              var smsObj = {};
              smsObj['email'] = this.email;
              smsObj['userId'] =UserID;
              var jsonString = JSON.stringify(smsObj);
              this.sendsmsapi=this.http.post<any>(this.data.WEBSERVICE + '/user/SendSMS', jsonString, { headers: { 'content-Type': 'application/json' } })
              .subscribe(data => {
                var result=data;
                if (data.error.error_data !="0") {
                  this.data.alert(result.error.error_msg, "danger");
                }
                else{
                  this.data.alert(result.error.error_msg, "success");
                  this.modalService.open(content, { centered: true });
              
                }

              })            
            
            }
            else {
              this.loader = false;
              this.otpBlock = true;
              $('.otp_segment').show();
              $('.otp_btn').show();
              $('.login_btn').hide();
              $('#loginInputOTP').focus();
            }
          }
        }, error => {
          this.loader = true;
        })
    }
    else {
      this.error = true;
    }
  }
  resendOtp(){
    var UserId=localStorage.getItem('UserID');
    var smsObj = {};
    smsObj['email'] = this.email;
    smsObj['userId'] =UserId;
    var jsonString = JSON.stringify(smsObj);
    this.sendsmsapi=this.http.post<any>(this.data.WEBSERVICE + '/user/SendSMS', jsonString, { headers: { 'content-Type': 'application/json' } })
    .subscribe(data => {
      var result=data;
      if (data.error.error_data !="0") {
        this.data.alert(result.error.error_msg, "danger");
      }
      else{
        this.data.alert(result.error.error_msg, "success");
      ///  this.modalService.open(content, { centered: true });
    
      }

    })
  }
  sendloginotp(){

    if (this.otplogin.length == 5) {
      var UserId=localStorage.getItem('UserID');
    var OtpObj = {};
    OtpObj['userId'] =UserId;
    OtpObj['otp'] = this.otplogin;
    var jsonString = JSON.stringify(OtpObj);
    this.sendsmsapi=this.http.post<any>(this.data.WEBSERVICE + '/user/VerifyOtp', jsonString, { headers: { 'content-Type': 'application/json' } })
    .subscribe(data => {
      var result=data;
      if (data.error.error_data !="0") {
        this.data.alert(result.error.error_msg, "danger");
      }
      else{
        this.data.alert(result.error.error_msg, "success");
        this.modalService.dismissAll();
        this.setLoginData(data);
        
      }

    })            
    }
  }
// Countdown(){
//  var now = new Date().getTime();
//  var starttime = Math.round(now/1000);
// var  countDowntime=starttime + 3600;
//  var x = setInterval(function() {
//   var now = new Date().getTime();
//   var starttime = Math.round(now/1000);
//  this.distance=countDowntime - starttime;

//  if(this.distance == 20){
//   $('#logoutWarn').click();
//  }
//  else if(this.distance ==0){
//   localStorage.clear();
//    this.route.navigateByUrl('/login');
//  }
//  },1000)
// }

  loginThroughOtp() {
    var otpObj = {};
    otpObj['email'] = this.email;
    otpObj['otp'] = this.otp;
    var jsonString = JSON.stringify(otpObj);
    // wip(1);
  this.loginotpApi=this.http.post<any>(this.data.WEBSERVICE + '/user/CheckTwoFactor', jsonString, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .subscribe(response => {
        // wip(0);
        var result = response;
        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {



          this.setLoginData(result);
        }
      },
        reason => {
          this.data.alert(reason, 'danger')
        });
  }

  setLoginData(data) {
    
    this.userId = data.userResult.userId;
    let body = new URLSearchParams();
    body.set('username', this.userId);
    body.set('password', this.password);
    localStorage.setItem('password',this.password);
    body.set('grant_type', 'password');
    let options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('authorization', 'Basic cGF5Yml0by13ZWItY2xpZW50OlB5Z2h0bzM0TEpEbg==')
    };
  this.setlogdataApi=  this.http.post<any>(this.data.WEBSERVICE + '/oauth/token', body.toString(), options)
      .subscribe(dataAuth => {
        this.accessToken = dataAuth.access_token;
        this.refreshToken = dataAuth.refresh_token;
        this.expiresIn = dataAuth.expires_in;
        localStorage.setItem('access_token', this.accessToken);
        localStorage.setItem('refresh_token', this.refreshToken);
        var expiresTime = this.expiresIn;
        expiresTime = expiresTime*100000;
        var start_time = $.now();
        var expiresIn = start_time + expiresTime;
        localStorage.setItem('expires_in', expiresIn);
        var userObj = {};
        userObj['userId'] = this.userId;
        var userJsonString = JSON.stringify(userObj);
        this.http.post<any>(this.data.WEBSERVICE + '/user/GetUserDetails', userJsonString, { headers: { 'Content-Type': 'application/json', 'authorization': 'BEARER ' + this.accessToken } })
          .subscribe(dataRecheck => {
            if (dataRecheck.error.error_data == '0') {
              localStorage.setItem('user_name', dataRecheck.userListResult[0].fullName);
              localStorage.setItem('user_id', dataRecheck.userListResult[0].userId);
              localStorage.setItem('phone', dataRecheck.userListResult[0].phone);
              localStorage.setItem('email', dataRecheck.userListResult[0].email);
              localStorage.setItem('address', dataRecheck.userListResult[0].address);
              localStorage.setItem('profile_pic', dataRecheck.userListResult[0].profilePic);
              localStorage.setItem('check_id_verification_status', 'true');
              localStorage.setItem('selected_currency', 'btc');
              localStorage.setItem('buying_crypto_asset', 'btc');
              localStorage.setItem('selling_crypto_asset', 'usdt');
              this.cookie.set('access_token', localStorage.getItem('access_token'), 60);
              this.route.navigateByUrl('/dashboard');
              this.data.alert('Login Successful!', 'success');
              this.data.Countdown();
            }
          }, reason => {
            // wip(0);
            this.data.alert(reason, 'danger');
          });
      }, reason => {
        // wip(0);
        this.data.alert(reason, 'danger');
      });

  }
 
showHide() {
    $(".showHide-password").each(function () {
      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
  }
  ngOnDestroy(){
if(this.loginapi!=undefined){
  this.loginapi.unsubscribe();
}
if(this.loginotpApi!=undefined){
  this.loginotpApi.unsubscribe();
}
if(this.setlogdataApi!=undefined){
  this.setlogdataApi.unsubscribe();
}
if(this.loginotpApi!=undefined){
  this.loginotpApi.unsubscribe();
}

  }

}