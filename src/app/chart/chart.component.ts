import { Component, OnInit, DoCheck } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CoreDataService } from "../core-data.service";
import { OrderBookComponent } from "../order-book/order-book.component";
import { StopLossComponent } from "../stop-loss/stop-loss.component";
import { TradesComponent } from "../trades/trades.component";
import * as $ from "jquery";
import { DashboardComponent } from "../dashboard/dashboard.component";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";

@Component({
  selector: "app-chart",
  templateUrl: "./chart.component.html",
  styleUrls: ["./chart.component.css"]
})
export class ChartComponent implements OnInit {
  filePath: string;
  fileDir: string;
  url: any = null;
  chartDataInit: any;
  chartData: any;
  selected: any;
  indicatorStatus: any = 0;
  indicatorName: any = "";
  apiUrl: string;
  apiData: any;
  loader: boolean;
  errorText: string;
  selectedBuyingCryptoCurrencyName: string;
  selectedSellingCryptoCurrencyName: string;
  selectedCryptoCurrencySymbol: string;
  selectedBuyingAssetText: string;
  selectedSellingAssetText: string;
  currencyListEur: any = 0;
  currencyListBtc: any = 0;
  currencyListEth: any = 0;
  currencyListUsd: any = 0;
  buyPriceText: any;
  chartD: any = "d";
  dateD: any = "1m";
  indicatorGroup1: any;
  indicatorGroup2: any;
  highValue;
  lowValue;
  cur: any;
  tools: boolean;
  chartlist: any;
  ctpdata: any = 0;
  ltpdata: any = 0;
  lowprice: any = 0;
  highprice: any = 0;
  rocdata: number = 0;
  action: any;
  volumndata: any = 0;
  act: any;
  react: boolean;
  buyingAssetIssuer: string;
  sellingAssetIssuer: string;
  //rocact:any;
  rocreact: boolean;
  droc: any;
  negetive: boolean;
  Tonight: boolean;
  Tomorning: boolean;
  filterCurrency: any;
  logic: boolean;
  flag: boolean;
  Cname: any;
  testval = [];
  currency_code: any;
  base_currency: any;
  assetpairbuy: string;
  assetpairsell: string;
  responseBuySell: any;
  header: any;
  assets: any;
  currencyId: any;
  getlang: any;
  private currencyapi: Subscription;
  constructor(
    private http: HttpClient,
    public data: CoreDataService,
    private orderBook: OrderBookComponent,
    private stoploss: StopLossComponent,
    private trade: TradesComponent,
    public dash: DashboardComponent,
    private translate: TranslateService

  ) {
    translate.addLangs(['Turkish', 'English']);
    this.getlang = localStorage.getItem("selectedlang");
    if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang('Turkish');
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    else {

      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : this.getlang);
    }
  }

  ngOnInit() {

    this.currency_code = 'BTC';
    this.base_currency = 'TRY';
    localStorage.setItem("buying_crypto_asset", 'BTC');
    localStorage.setItem("selling_crypto_asset", 'TRY');
    this.data.selectedSellingAssetText = this.base_currency;
    this.data.selectedBuyingAssetText = this.currency_code;
    this.data.cur = this.data.selectedBuyingAssetText === "TRY"
      ? "$"
      : this.data.selectedSellingAssetText + " ";
    this.selectedSellingCryptoCurrencyName = this.data.selectedSellingCryptoCurrencyName = this.currency_code + this.base_currency;
    this.selectedBuyingCryptoCurrencyName = this.data.selectedBuyingCryptoCurrencyName = this.base_currency + this.currency_code;
    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText = this.currency_code;
    this.selectedSellingAssetText = this.data.selectedSellingAssetText = this.base_currency;
    this.selectedCryptoCurrencySymbol = this.data.selectedCryptoCurrencySymbol =
      "./assets/img/currency/" +
      this.data.selectedSellingAssetText.toUpperCase() +
      ".svg";
    $('#' + this.dateD).addClass('active');

    $('.reset').click();
    this.tools = true;
    $('#dropHolder').css('overflow', 'scroll');
    $(window).resize(function () {
      var wd = $('#chartHolder').width();
      $('#dropHolder').width(wd);
      $('#dropHolder').css('overflow', 'scroll');
    });

    var url = 'https://api.tomya.com/StreamingApiTest/rest/trendsTradeGraphFor24Hours/' + this.currency_code + '/' + this.base_currency;
    if (this.data.source4 != undefined) {
      this.data.source4.close();
    }
    if (this.data.source5 != undefined) {
      this.data.source5.close();
    }
    if (this.data.source6 != undefined) {
      this.data.source6.close();
    }
    this.data.source4 = new EventSource(url);

    var result: any = new Object();
    this.data.source4.onmessage = (event: MessageEvent) => {
      result = event.data;
      console.log('result+++++++',result);
      
      if (result != '') {
        result = JSON.parse(event.data);
        if (this.currency_code == 'BTC' || this.base_currency == 'TRY' || this.base_currency == 'USDT') {
          this.chartlist = result[0];
          this.ctpdata = this.data.ctpdata = this.chartlist.ctp;
          this.ltpdata = this.data.ltpdata = this.chartlist.ltp;
          this.lowprice = this.data.lowprice = this.chartlist.lowPrice;
          this.highprice = this.data.highprice = this.chartlist.highPrice;
          this.act = this.data.ACTION = this.chartlist.action;
          this.data.rocdata = parseFloat(this.chartlist.roc).toFixed(4);
          if (this.data.rocdata > 0) {
            this.data.rocreact = true;
          }
          else {
            this.data.rocreact = false;
          }
          if (this.data.rocdata < 0) {
            this.data.negetive = true;
          }
          else {
            this.data.negetive = false;
          }
          this.data.volumndata = parseFloat(this.chartlist.volume).toFixed(2);
          if (this.act == 'sell') {
            this.data.react = true;
          }
          else {
            this.data.react = false;
          }
        }
        else {
          this.chartlist = result[0];
          this.ctpdata = this.data.ctpdata = this.chartlist.ctp;
          this.ltpdata = this.data.ltpdata = this.chartlist.ltp;
          this.lowprice = this.data.lowprice = this.chartlist.lowPrice;
          this.highprice = this.data.highprice = this.chartlist.highPrice;
          this.act = this.data.ACTION = this.chartlist.action;
          this.data.rocdata = this.chartlist.roc;
          if (this.data.rocdata > 0) {
            this.data.rocreact = true;
          }
          else {
            this.data.rocreact = false;
          }
          if (this.data.rocdata < 0) {
            this.data.negetive = true;
          }
          else {
            this.data.negetive = false;
          }
          this.data.volumndata = this.chartlist.volume;
          if (this.data.rocdata >= 0) { this.data.rocreact = true }
          if (this.data.act == 'sell') {
            this.data.react = true;
          }
          else {
            this.data.react = false;
          }
        }
      }
      else {

        this.ctpdata = this.data.ctpdata = '0';
        this.ltpdata = this.data.ltpdata = '0';
        this.lowprice = this.data.lowprice = '0';
        this.highprice = this.data.highprice = '0';
        this.data.rocdata = '0';
        //  this.rocdata=this.data.ACTION =0;
        this.volumndata = this.data.volumndata = 0;
      }

    }

    //  })
  }

  Selectedlangulage(lang) {
    localStorage.getItem('selectedlang');
  }

  changemode() {
    if (this.data.changescreencolor == true) {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#fefefe");
      $(".btn")
        .removeClass("bg-black")
        .css("background-color", "#dedede");
      $(".btn").css("border-color", "#b3b4b4");
      $(".btn").css("color", "#000");
      $(".charts-tab.active").css("background-color", "#fefefe");
    } else {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#16181a");
      $(".btn")
        .removeClass("bg-black")
        .css("background-color", "#564fdc");
      $(".btn").css("border-color", "transparent");
      $(".btn").css("color", "#fff");
      $(".charts-tab.active").css("background-color", "#242e3e");
    }
  }

  ngDoCheck() {
    this.changemode();
  }

  getNewCurrency() {

    this.currencyapi = this.http.get<any>(this.data.WEBSERVICE + '/userTrade/GetCurrencyDetails')
      .subscribe(responseCurrency => {
        this.filterCurrency = responseCurrency;
        var mainArr = [];
        this.header = this.filterCurrency.Header;
        this.assets = this.filterCurrency.Values;
      }
      )
  }
  //clear indicators
  destroy(): void {
    this.selected = null;
  }
  ngOnDestroy() {
    if (this.data.source4 != undefined) {
      this.data.source4.close();
    }
    if (this.currencyapi != undefined) {
      this.currencyapi.unsubscribe();
    }

  }
}