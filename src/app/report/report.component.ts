import { Component, OnInit } from "@angular/core";
import * as $ from "jquery";
import { CoreDataService } from "../core-data.service";
import { HttpClient } from "@angular/common/http";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-report",
  templateUrl: "./report.component.html",
  styleUrls: ["./report.component.css"]
})
export class ReportComponent implements OnInit {
  fiancialyear: any;
  reportlist: any;
  fromdate: any;
  usr: any;
  model: any;
  startdata: any;
  sldata: any;
  transtiondata: any;
  price: any;
  keys: any;
  sumfdt: any;
  sumtdt: any;
  keyval: any;
  results: any;
  headerdata: any;
  fromDate: any;
  reportid: any;
  datastore: any;
  reportarray: any = "selectreport";
  selectyear: any = "fyear";
  reportkeys: any;
  todate: any;
  toDate: any;
  fundingheader: any;
  year: number;
  month: number;
  newheader: any;
  datavalue: any;
  selectpdf: any = "selectoption";
  downloadUrl: any = "javascript:void(0)";
  // day: number;
  ID: any;
  item: any;
  public show: any;
  repu: any;
  public buttonName: any = "Show";

  /*****date picker max and min date*******/
  currentDate: any = new Date().toISOString().slice(0, 10);
  currentYear: any = new Date().getFullYear();
  currentMonth: any = new Date().getMonth() + 1;
  currentDay: any = new Date().getDate();

  /*********for showing export csv file button*******/
  showExportButton = 0;

  /********pagination related variables**********/
  totalCount = 0;
  // pageNo=1;
  noOfItemsPerPage = 10;
  showingRowValue = 10;
  showPrevButton = 0;
  showNextButton = 0;
  pagiArr = [];
  pagiLength: any;
  page = 1;
  getlang: any;
  reportlist1:any;
  reportlist2:any;
  reportlist3:any;
  reportlist4:any;
  reportlist5:any;
  reportlist6:any;

  constructor(
    private http: HttpClient,
    private data: CoreDataService,
    public translate: TranslateService
  ) {
    translate.addLangs(["Turkish", "English"]);
    this.getlang = localStorage.getItem("selectedlang");
    if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang("Turkish");
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : "Turkish"
      );
    } else {
      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : this.getlang
      );
    }
  }

  ngOnInit() {
    this.getreportdata();
    this.financialyear();
  }
  Selectedlangulage(lang) {
    localStorage.getItem("selectedlang");
  }

  reset() {
    this.fromdate = this.fromdate = this.fromdate = "";

    this.reportarray = this.selectyear = "";

    this.todate = this.todate = this.todate = "";
    this.model = this.model = this.model = "";
    $(function() {
      $("input.form-control").val("");
    });
  }

  fundBuy(content) {
    if (this.reportarray && this.fromdate && this.todate) {
      open(content);
    } else {
      alert("Please Fillup all the given field");
    }
  }

  getreportdata() {
    this.http
      .get<any>(this.data.REPORTSERVISE + "viewAllMaster")
      .subscribe(response => {
        this.reportkeys = Object.keys(response);
        this.reportlist1 = response;
        this.reportlist2 = [{'dailytransaction':'Günlük İşlem Özeti'} ,
        {'tradeHistory':'İşlem Geçmişi'},
        {'poartfolioSummry':'Portföy Özeti'},
        {'wallettransactionsummry':'Cüzdan İşlem Özeti'},
        {'transactionchrgsummry':'İşlem Ücretlendirme Özeti'},
      {'gainLoss':'Kar Zarar Raporu'}];
      });
  }

  finddata(datastore, reportarray, pageNo) {
    this.page = pageNo != 0 ? pageNo : this.page;
    var usr = localStorage.getItem("user_id");
    this.repu = reportarray;

    var ds = this.fromdate;
    var dp = this.todate;

    if (ds == undefined && dp == undefined) {
      this.sumfdt = "1" + "1" + this.selectyear;
      this.sumtdt = "31" + "12" + this.selectyear + 1;
    } else {
      this.fromDate = ds.day + "-" + ds.month + "-" + ds.year;
      this.toDate = dp.day + "-" + dp.month + "-" + dp.year;

      this.sumfdt = ds.month + ds.year;
      this.sumtdt = dp.month + dp.year;
    }
    if (this.repu == 6) {
      this.sumfdt = "0";
      this.sumtdt = "1";
    }
    if (
      this.sumtdt > this.sumfdt ||
      (this.sumfdt == this.sumtdt && dp.day > ds.day) ||
      dp.year > ds.year
    ) {
      if (this.repu != 6) {
        this.http
          .get<any>(
            this.data.REPORTSERVISE +
              "viewRepotDetails/" +
              usr +
              "/" +
              reportarray +
              "/" +
              this.fromDate +
              "/" +
              this.toDate +
              "/" +
              2018 +
              "/" +
              this.page
          )
          .subscribe(response => {
            this.headerdata = Object.keys(response);

            this.keyval = Object.values(response[0]);
            this.datastore = this.keyval[0];

            this.fundingheader = this.keyval[1];

            this.results = Object.keys(this.fundingheader);
            this.showExportButton = 1;
            this.totalCount = response[0]["row count"][0];

            this.pagiArr = [];
            for (var i = 1; i <= Math.ceil(this.totalCount / 25); i++) {
              this.pagiArr.push(i);
            }
            this.pagiLength = this.pagiArr.length;
          });
      } else {
        this.fromDate = "0";
        this.toDate = "0";

        this.http
          .get<any>(
            this.data.REPORTSERVISE +
              "viewRepotDetails/" +
              usr +
              "/" +
              reportarray +
              "/" +
              this.fromDate +
              "/" +
              this.toDate +
              "/" +
              this.selectyear +
              "/" +
              this.page
          )
          .subscribe(response => {
            this.headerdata = Object.keys(response);
            this.keyval = Object.values(response[0]);
            this.datastore = this.keyval[0];
            this.fundingheader = this.keyval[1];
            this.results = Object.keys(this.fundingheader);
            this.showExportButton = 1;
            this.totalCount = response[0]["row count"][0];

            this.pagiArr = [];
            for (var i = 1; i <= Math.ceil(this.totalCount / 25); i++) {
              this.pagiArr.push(i);
            }
            this.pagiLength = this.pagiArr.length;
          });
      }
      if (this.repu != 6) {
        this.downloadUrl =
          this.data.REPORTSERVISE +
          "viewRepotDetails/" +
          localStorage.getItem("user_id") +
          "/" +
          this.repu +
          "/" +
          this.fromDate +
          "/" +
          this.toDate +
          "/2018";
      } else {
        this.downloadUrl =
          this.data.REPORTSERVISE +
          "viewRepotDetails/" +
          localStorage.getItem("user_id") +
          "/" +
          this.repu +
          "/0/0/" +
          this.selectyear;
      }
    } else {
      alert("From Date is greater than To date");
      this.sumtdt = "";
      this.sumfdt = "";
    }
  }

  OnChange(val) {
    if (val == 6) this.show = 1;
    else {
      this.show = 0;
    }
  }

  exportTableToCSV(table, filename) {
    var $headers = $(table).find("tr:has(th)"),
      $rows = $(table).find("tr:has(td)"),
      tmpColDelim = String.fromCharCode(11),
      tmpRowDelim = String.fromCharCode(0),
      colDelim = '","',
      rowDelim = '"\r\n"';
    // Grab text from table into CSV formatted string
    var csv = '"';
    csv += formatRows($headers.map(grabRow));
    csv += rowDelim;
    csv += formatRows($rows.map(grabRow)) + '"';
    var csvData =
      "data:application/csv;charset=utf-8," + encodeURIComponent(csv);
    $("#downloadCSVBtn").attr({
      download: filename,
      href: csvData
    });
    function formatRows(rows) {
      return rows
        .get()
        .join(tmpRowDelim)
        .split(tmpRowDelim)
        .join(rowDelim)
        .split(tmpColDelim)
        .join(colDelim);
    }
    function grabRow(i, row) {
      var $row = $(row);
      var $cols = $row.find("td");
      if (!$cols.length) $cols = $row.find("th");
      return $cols
        .map(grabCol)
        .get()
        .join(tmpColDelim);
    }
    function grabCol(j, col) {
      var $col = $(col),
        $text = $col.text();
      return $text.replace('"', '""'); // escape double quotes
    }
  }

  pager(pg) {
    this.page = pg;
    this.finddata(this.datastore, this.reportarray, 0);
  }

  pagerNext(pg) {
    pg++;
    this.page = pg;
    this.finddata(this.datastore, this.reportarray, 0);
  }

  pagerPre(pg) {
    pg--;
    this.page = pg;
    this.finddata(this.datastore, this.reportarray, 0);
  }

  financialyear() {
    this.http
      .get<any>(this.data.REPORTSERVISE + "financialYearList")
      .subscribe(response => {
        this.fiancialyear = response;
      });
  }

  pagi(num) {
    this.page = parseInt(num);
    this.finddata(this.datastore, this.reportarray, 0);
  }

  downloadCsvFromBackend() {
    if (this.repu != 6) {
      var downloadUrl =
        this.data.REPORTSERVISE +
        "viewRepotDetails/" +
        localStorage.getItem("user_id") +
        "/" +
        this.repu +
        "/" +
        this.fromDate +
        "/" +
        this.toDate +
        "/2018";
    } else {
      var downloadUrl =
        this.data.REPORTSERVISE +
        "viewRepotDetails/" +
        localStorage.getItem("user_id") +
        "/" +
        this.repu +
        "/0/0/" +
        this.selectyear;
    }
    this.http.get<any>(downloadUrl).subscribe(response => {});
  }
}
