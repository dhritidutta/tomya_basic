import { Component, OnInit, DoCheck } from "@angular/core";
import { TradesComponent } from "../trades/trades.component";
import * as $ from "jquery";
import { CoreDataService } from "../core-data.service";
import { Router } from "@angular/router";
import { DashboardComponent } from "../dashboard/dashboard.component";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from 'rxjs';
import { StopLossComponent } from "../stop-loss/stop-loss.component";
import { TranslateService } from "@ngx-translate/core";
export interface Todo {
  id: number | string;
  createdAt: number;
  value: string;
}

@Component({
  selector: "app-order-book",
  templateUrl: "./order-book.component.html",
  styleUrls: ["./order-book.component.css"]
})

export class OrderBookComponent implements OnInit {
  buyingAssetIssuer: string;
  sellingAssetIssuer: string;
  bidBody: string;
  askBody: string;
  orderBookUrl: string;
  count: any;
  lowprice: any;
  highprice;
  any = 0;
  act:any;
  rocdata:any;
  chartlist: any = 0;
  getlang:any;
  ctpdata: any = 0;
  ltpdata: any = 0;
  marketTradeBodyHtml: any;
  private _todos = new BehaviorSubject<Todo[]>([]);
  private dataStore: { todos: Todo[] } = { todos: [] };
  readonly todos = this._todos.asObservable();
  public biddata:any;
  public askdata:any;
   public source1 :any;
  constructor(
    public data: CoreDataService,
    private route: Router,
    public dash: DashboardComponent,
    private http: HttpClient,
    private trade:TradesComponent,
    private stoploss:StopLossComponent,
    public translate:TranslateService

  ) {
    switch (this.data.environment) {
      case "live":
        this.orderBookUrl = "https://mainnet.tomya.com/"; //live
        break;

      case "dev":
        this.orderBookUrl =
          ""; //dev
        break;

      case "uat":
        this.orderBookUrl = ""
        break;

      case "demo":
        this.orderBookUrl = "https://mainnet.tomya.com/"; //demo
        break;
    }
    translate.addLangs(['Turkish', 'English']);
    this.getlang = localStorage.getItem("selectedlang");
      if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang('Turkish');
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    else {

      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : this.getlang);
    }
  }

  urlBid: any;
  urlAsk: any;
  rocreact:any;
  selectedBuyingCryptoCurrencyName: string;
  selectedSellingCryptoCurrencyName: string;
  sellingAssetType: string;
  buyingAssetType: string;
  volumndata:any;
  react:any;
  negetive:any;
  source:any;
  token: any;
  marketTradeRecords: any;
  itemcount: any;

  ngOnInit() {
    this.selectedSellingCryptoCurrencyName = this.data.selectedSellingCryptoCurrencyName;
    this.selectedBuyingCryptoCurrencyName = this.data.selectedBuyingCryptoCurrencyName;
    this.currency_code = 'BTC';
    this.base_currency = 'TRY';

      this.serverSentEventForOrderbookAsk();
      this.getMarketTradeBuy();
  }

  Selectedlangulage(lang) {
    localStorage.getItem('selectedlang');
  }

  changemode() {
    if (this.data.changescreencolor == true) {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#fefefe");
      $(".sp-highlow").css("background-color", "#d3dddd");
      $(".sp-highlow").css("color", "Black");
      $(".border-col").css("border-color", "#d3dddd");
      $("th").css({ "background-color": "#d3dddd", color: "#273338" });
      $(".text-left").css("color", "black");
      $(".text-right").css("color", "black");
    } else {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#16181a");
      $(".sp-highlow").css("background-color", "#273338");
      $(".sp-highlow").css("color", "yellow");
      $(".border-col").css("border-color", "#273338");
      $("th").css({ "background-color": "#273338", color: "#d3dddd" });
      $(".text-left")
        .css("color", "")
        .css("color", "rgb(153, 152, 152)");
      $(".text-right").css("color", "rgb(153, 152, 152)");
    }
  }

  ngDoCheck() {
    this.changemode();
    var randomNoForFlashArr = [];
    var arraylength = 2;
    var valcurrency = this.data.ctpdata;
    if (valcurrency == undefined) {
      this.ctpdata = this.chartlist.CTP;
      this.ltpdata = this.chartlist.LTP;
      this.lowprice = this.chartlist.LOW_PRICE;
      this.highprice = this.chartlist.HIGH_PRICE;
    } else {
      this.ctpdata = this.data.ctpdata;
      this.ltpdata = this.data.ltpdata;
      this.lowprice = this.data.lowprice;
      this.highprice = this.data.highprice
    }
    var randomNo = this.randomNoForOrderBook(0, arraylength);
    randomNoForFlashArr.push(randomNo);
    for (var i = 0; i < arraylength; i++) {
      if (!$.inArray(i, randomNoForFlashArr)) {
        if (i == 0) {
          document.getElementById("pricered").style.display = "inline-block";

          document.getElementById("pricegreen").style.display = "none";
        } else {
          document.getElementById("pricered").style.display = "none";

          document.getElementById("pricegreen").style.display = "inline-block";
        }
      }
    }

  }

  filterCurrency: any;
  header: any;
  assets: any;

  getNewCurrency() {
    this.http.get<any>('https://api.tomya.com/CacheService/api/getData?Name=Assets')
      .subscribe(responseCurrency => {
        this.filterCurrency = JSON.parse(responseCurrency.value);
        var mainArr = [];
        this.header = this.filterCurrency.Header;
        this.assets = this.filterCurrency.Values;

      }
      )
  }

  currency_code: any;
  base_currency: any;
  selectedBuyingAssetText: string;
  selectedSellingAssetText: string;
  assetpairbuy: string;
  assetpairsell: string;
  responseBuySell: any;
  cur:any

  buySellCode(item) {
    this.currency_code = item.currencyCode;
    this.base_currency = item.baseCurrency;
    this.data.selectedSellingAssetText = this.base_currency;
    this.data.selectedBuyingAssetText = this.currency_code;
   this.data.selectedBuyingCryptoCurrencyName =this.base_currency+ this.currency_code;
    this.data.selectedSellingCryptoCurrencyName = this.currency_code + this.base_currency;
    this.selectedBuyingAssetText = item.currencyCode;
    this.selectedSellingAssetText = item.baseCurrency;
    localStorage.setItem("buying_crypto_asset", this.currency_code.toLocaleLowerCase());
    localStorage.setItem("selling_crypto_asset", this.base_currency.toLocaleLowerCase());
    this.data.cur = this.selectedSellingAssetText;
    this.assetpairsell = item.currencyCode + item.baseCurrency;
    this.assetpairbuy = item.baseCurrency + item.currencyCode;
    this.data.selectedSellingCryptoCurrencyissuer = "";
    this.data.selectedBuyingCryptoCurrencyissuer = "";
    // this.http.get<any>('https://api.tomya.com/CacheService/api/getData?Name=AssetIssuer')
    //   .subscribe(responseBuySell => {
    //    this.responseBuySell = JSON.parse(responseBuySell.value);
    //     var x = this.responseBuySell.length;
        // for (var i = 0; i < x ; i++) {
        //   if (this.assetpairsell == this.responseBuySell[i].assetPair) {
        //     this.data.selectedSellingCryptoCurrencyissuer = this.responseBuySell[i].issuer;

        //   }
        //   else if (this.assetpairbuy == this.responseBuySell[i].assetPair) {
        //     this.data.selectedBuyingCryptoCurrencyissuer = this.responseBuySell[i].issuer;
        //   }
        //   else if (this.data.selectedSellingCryptoCurrencyissuer != "" && this.data.selectedBuyingCryptoCurrencyissuer != "") {
        //     break;
        //   }
        // }
        this.stoploss.update();
        this.stoploss.market = false;
        this.serverSentEventForOrderbookAsk();
        this.getMarketTradeBuy();
        this.trade.reload();
        this.stoploss.onlyBuyAmount = this.stoploss.onlyBuyPrice = this.stoploss.onlyBuyTotalPrice = 0;
        this.stoploss.onlySellAmount = this.stoploss.onlySellPrice = this.stoploss.onlySellTotalPrice = 0;
        $('.reset').click();
        this.trade.myTradeDisplay(0);
        $('#trade').click();
        $('#dropHolder').css('overflow', 'scroll');
        $(window).resize(function () {
          var wd = $('#chartHolder').width();
          $('#dropHolder').width(wd);
          $('#dropHolder').css('overflow', 'scroll');
        });
     
        var url='https://api.tomya.com/StreamingApiTest/rest/trendsTradeGraphFor24Hours/' + this.currency_code + '/' + this.base_currency;
        if (this.source1 != undefined ) {
          this.source1.close();
        }
        if (this.data.source4 != undefined ) {
          this.data.source4.close();
        }
      this.source1 = new EventSource(url);
      this.urlAsk = url;
      var result: any = new Object();
      this.source1.onmessage = (event: MessageEvent) => {
        result = event.data;
        result = JSON.parse(event.data);
        if (result.length !=0) {
          
          if(this.currency_code == 'BTC' || this.base_currency == 'TRY' || this.base_currency == 'USDT' || this.base_currency == 'BTC'){
            this.chartlist = result[0];
            this.ctpdata = this.data.ctpdata = this.chartlist.ctp;
            this.ltpdata = this.data.ltpdata = this.chartlist.ltp;
            this.lowprice = this.data.lowprice = this.chartlist.lowPrice;
            this.highprice = this.data.highprice = this.chartlist.highPrice;
            this.act = this.data.ACTION = this.chartlist.action;
            this.data.rocdata = parseFloat(this.chartlist.roc).toFixed(4);
            if (this.data.rocdata > 0) {
              this.rocreact = true;
            }
            else {
              this.data.rocreact = false;
            }
            if (this.data.rocdata < 0) {
              this.data.negetive = true;
            }
            else {
              this.data.negetive = false;
            }
            this.data.volumndata =parseFloat(this.chartlist.volume).toFixed(2);
            if (this.data.rocdata >= 0) { this.rocreact = true }
            if (this.data.act == 'sell') {
              this.data.react = true;
            }
            else {
              this.data.react = false;
            }
          }
          else{
            this.chartlist = result[0];
            this.ctpdata = this.data.ctpdata = this.chartlist.ctp;
            this.ltpdata = this.data.ltpdata = this.chartlist.ltp;
            this.lowprice = this.data.lowprice = this.chartlist.lowPrice;
            this.highprice = this.data.highprice = this.chartlist.highPrice;
            this.act = this.data.ACTION = this.chartlist.action;
            this.data.rocdata = this.chartlist.roc;
            if (this.data.rocdata > 0) {
              this.data.rocreact = true;
            }
            else {
              this.data.rocreact = false;
            }
            if (this.data.rocdata < 0) {
              this.data.negetive = true;
            }
            else {
              this.data.negetive = false;
            }
             this.data.volumndata = this.chartlist.volume;
            // if (this.data.rocdata >= 0) { this.rocreact = true }
            if (this.data.act == 'sell') {
              this.data.react = true;
            }
            else {
              this.data.react = false;
            }
          }
        }
        else{
          this.ctpdata=this.data.ctpdata = '0';
          this.ltpdata = this.data.ltpdata = '0';
              this.lowprice = this.data.lowprice= '0';
              this.highprice=this.data.highprice = '0';
              this.rocdata=this.data.rocdata ='0';
              this.volumndata=this.data.volumndata = 0; 
        }
      
       }

      //})
  }

  randomNoForOrderBook(minVal: any, maxVal: any): number {
    var minVal1: number = parseInt(minVal);
    var maxVal1: number = parseInt(maxVal);
    return Math.floor(Math.random() * (maxVal1 - minVal1 + 2) + minVal1);
  }

  getMarketTradeBuy() {
    $('#marketTradeBody').html('<tr><td colspan="3" class="text-center"><img src="./assets/svg-loaders/three-dots.svg" alt="" width="50"></td></tr>');
    var result: any;
    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
    this.selectedSellingAssetText = this.data.selectedSellingAssetText;
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;//'GBDWSH2SAZM3U5FR6LPO4E2OQZNVZXUAZVH5TWEJD64MGYJJ7NWF4PBX';
    var url ='https://api.tomya.com/StreamingApi/rest/TradeHistory?baseAssetCode=' + this.data.selectedSellingCryptoCurrencyName.toUpperCase() +'&counterAssetCode=' + this.data.selectedBuyingCryptoCurrencyName.toUpperCase() + '&limit=' + 50;
    if (this.source != undefined ) {
      this.source.close();
    }
    this.source= new EventSource(url);
    this.source.addEventListener('message', event => {
      result = JSON.parse(event.data);
      this.marketTradeBodyHtml = '';
      if(result.response !=null){
      var response = JSON.parse(result.response);
      this.marketTradeRecords=response._embedded.records;
      this.itemcount = this.marketTradeRecords.length;
      var arraylength = this.marketTradeRecords.length;
          if (arraylength != null) {
          this.marketTradeBodyHtml = '';
                  var randomNoForFlashArrM = [];
          var randomNo = this.randomNoForOrderBook(0, 10);
          randomNoForFlashArrM.push(randomNo);

          randomNoForFlashArrM.push(randomNo);

          for (var i = 0; i < this.marketTradeRecords.length; i++) {
           // var className = "text-green";
            if (!$.inArray(i, randomNoForFlashArrM)) {
            
              if (randomNo % 2 == 0) {
             var pickclass='.'+i+'newclassmarket';
              // alert('////'+randomNo+pickclass);
             // alert(pickclass);
               $(pickclass).addClass('bg-flash-red');

              } else if (randomNo % 2 != 0) {
                var pickclass='.'+i+'newclassmarket';
             //   alert('////////'+randomNo+pickclass);
               // var className = 'text-red';
               $(pickclass).addClass("bg-flash-green");
              }
            }
            
          }
        }
        else{
          
        }
      }
      else{
      }
      }, error => {
      })

  }

  serverSentEventForOrderbookAsk() {
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;
    this.token = localStorage.getItem('access_token');
    var url = "https://api.tomya.com/StreamingApiTest/rest/OrderBook?sellingAssetCode=" + this.data.selectedSellingCryptoCurrencyName + "&buyingAssetCode=" + this.data.selectedBuyingCryptoCurrencyName+ "&limit=50";

    if (this.data.source2 != undefined ) {
      this.data.source2.close();
    }
    if (this.token.length >= "null" || this.token.length >= 0) {
      this.data.source2 = new EventSource(url);
      this.urlAsk = url;
      var result: any = new Object();
      this.data.source2.onmessage = (event: MessageEvent) => {
        result = event.data;
        //result = JSON.parse(event.data);
        var response = JSON.parse(event.data);
        var askdata = [];
        this.askdata = response.asks;
        var biddata = [];
        this.biddata = response.bids;
        var bidHtml = "";
        var askHtml = "";
        if (this.askdata.length != 0 ) {
          var randomNoForFlashArr1 = [];
          var randomNo = this.randomNoForOrderBook(0,this.askdata.length);
          randomNoForFlashArr1.push(randomNo);
          for (var i=0; i<=10; i++) {
          if (!$.inArray(i,randomNoForFlashArr1)) {
            var pickclass1='.'+i+'newclass';

            $(pickclass1).addClass('bg-flash-red');

            }

           }
        } else {

        }
       // this.askBody = askHtml;
      //  $("#orderbookAskBody").html(this.askBody);
        if (this.biddata.length != 0 ) {
          if (biddata.length > 5) { //changed by sanu
            var bidLength = biddata.length;
          } else {
            var bidLength = biddata.length;
          }
          var randomNoForFlashArr = [];
          var randomNo = this.randomNoForOrderBook(0,this.biddata.length);
          randomNoForFlashArr.push(randomNo);
         // alert('kk'+randomNo);
          if (!$.inArray(0, randomNoForFlashArr)) {

            var pickclass='.'+randomNo+'newclassask';
         // alert('pp'+pickclass);
           $(pickclass).addClass('bg-flash-green');

            }
        for (var i=0; i<=10; i++) { //changed by sanu
              if (!$.inArray(i, randomNoForFlashArr)) {

             var pickclass='.'+i+'newclassask';
            //alert('pp'+pickclass);
            $(pickclass).addClass('bg-flash-green');

             }
            
           }
        } else {
          
        }
        

      };
    } else {
      this.data.source2.close();
    }
  }

  ngOnDestroy(){
    if (this.data.source2 != undefined) {
      this.data.source2.close();
    }
    if(this.source1 != undefined){
      this.source1.close();
    }
    if (this.source.close() != undefined){
      this.source.close();
    }
  }
}