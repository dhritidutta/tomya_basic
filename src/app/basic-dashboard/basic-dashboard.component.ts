import { Component, OnInit,DoCheck } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { CoreDataService } from '../core-data.service';
import { HttpClient } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as $ from "jquery";
import { TradesComponent } from '../trades/trades.component';
import { StopLossComponent } from '../stop-loss/stop-loss.component';
import{OrderBookComponent} from "../order-book/order-book.component";
import { Observable, Subscription } from 'rxjs';
import{ChartComponent} from "../chart/chart.component"
@Component({
  selector: 'app-basic-dashboard',
  templateUrl: './basic-dashboard.component.html',
  styleUrls: ['./basic-dashboard.component.css']
})
export class BasicDashboardComponent implements OnInit {
  getlang:any;
  currentRoute:any;
  userName: any;
  profilePic: any;
  token: any = localStorage.getItem('access_token');
  userid:any;
  imageLink:any;
  act:any
  screencolor: boolean;
  filterCurrency: any;
   header: any;
  assets: any;
  currency_code:any;
  base_currency:any;
  urlAsk: any;
//askdata:any;
 // biddata:any;
  selectedBuyingAssetText: string;
  selectedSellingAssetText: string;
  assetpairbuy: string;
  assetpairsell: string;
  responseBuySell: any;
 // cur:any
  myTradeTableDisplayStatus:any;
  //historyTableTr:any;
  loader: boolean;
  myOfferRecordsHtml: Observable<any>;
  modifyAmount: any;
  modifyPrice: any;
  modifyVolume: any;
  manageOfferId: any;
  modifyType: any;
  delOfferId: any;
  delTxnType: any;
  delPrice: any;
  source:any;
  marketTradeRecords: any;
  itemcount: any;
  marketTradeBodyHtml: any;
//  source1:any;
  ctpdata: any = 0;
  ltpdata: any = 0;
  lowprice: any = 0;
  highprice: any = 0;
  rocdata: number = 0;
  volumndata: any = 0;
  chartlist: any;
  rocreact: boolean;
  market: boolean;
  onlyBuyAmount: any;
  onlyBuyPrice: any;
  onlyBuyTotalPrice: any;
  onlySellAmount: any;
  onlySellPrice: any;
  onlySellTotalPrice: any;
  selectedCryptoCurrency: string;
  selelectedBuyingAssetBalance: string = "0";
  selelectedSellingAssetBalance: string = "0";
  currencyBalance;
  balencelist;
  limitPrice: any = 0;
  limitValue: any = 0;
  onlySellPrice1:any;
  onlyBuyPrice1:any;
private currencyapi:Subscription;
private tradeapi:Subscription;
private offerlistapi:Subscription;
private offerdtlapi:Subscription;
private checkoffer:Subscription;
private manageoffer:Subscription;
private trademanagerapi:Subscription;
private deleteoffer:Subscription
private byvalapi:Subscription;
private marketbuyapi:Subscription;
private offercheckapi:Subscription;
private createoffer:Subscription;
private geTamountSellapi:Subscription;
private getamountsell:Subscription;
private Offerpricecheckapi:Subscription;
private tradecreateOffer:Subscription;
private Userbalanceapi:Subscription;
  constructor(public _ChartComponent:ChartComponent,public _orderbook:OrderBookComponent,public trade:TradesComponent,public stoploss:StopLossComponent,public translate: TranslateService, private route: Router, private modalService: NgbModal, public data: CoreDataService, private http: HttpClient) { 
    this.route.events.subscribe(val => {
      this.currentRoute = val;
    });
    translate.addLangs(['Turkish', 'English']);
    this.getlang = localStorage.getItem("selectedlang");
    if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang('Turkish');
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : 'Turkish');
    }
    else {

      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(browserLang.match(/Turkish|English/) ? browserLang : this.getlang);
    }
    $("#myTradeBody").html(this.trade.historyTableTr);
  }

  ngOnInit() {
 // window.location.reload();
    this.route.navigate(['/basic-dashboard']);
    localStorage.setItem('dashboard','0');
    localStorage.setItem('basic-dashboard','1');
    this.currency_code=this.data.selectedBuyingAssetText = 'BTC';
    this.base_currency = 'TRY';
    this.data.selectedSellingAssetText = this.base_currency;
    this.data.selectedBuyingAssetText = this.currency_code;
    this.data.cur =this.data.selectedBuyingAssetText === "TRY"
    ? "$"
    : this.data.selectedSellingAssetText + " ";
    this.data.selectedBuyingCryptoCurrencyName =this.base_currency+ this.currency_code;
    this.data.selectedSellingCryptoCurrencyName = this.currency_code + this.base_currency;
     this.selectedBuyingAssetText = localStorage.getItem('buying_crypto_asset');
    this.selectedSellingAssetText = localStorage.getItem('selling_crypto_asset');
    //this.data.selectedSellingCryptoCurrencyissuer = "GCJK5PBMMAGO3SIH3BVDKNSDNTJHETXFQEOSYUILXFWSDNCA3LTCSI4U";
    // this.data.selectedBuyingCryptoCurrencyissuer = "GBXI2ZBD44SZQ7OVCJXEDOTPEMIBWNJ5VQFCFROW5G3BSMWPLLWSSXNS";
    this.myTradeTableDisplayStatus = 0;
    this._orderbook.serverSentEventForOrderbookAsk();
    //this.getMarketTradeBuy();
    this.trade.GetTradeDetail();
   // this.trade.reload();
   this.trade.getOfferLists();
    var url='https://api.tomya.com/StreamingApiTest/rest/trendsTradeGraphFor24Hours/' + this.currency_code + '/' + this.base_currency;
    if (this.data.source5 != undefined ) {
        this.data.source5.close();
      }
    this.data.source5= new EventSource(url);
    
    var result: any = new Object();
    this.data.source5.onmessage = (event: MessageEvent) => {
      result = event.data;
      if (result != '') {
        result = JSON.parse(event.data);
        if(this.currency_code == 'BTC' || this.base_currency == 'TRY' || this.base_currency == 'USDT'){
          this.chartlist = result[0];
          this.ctpdata = this.data.ctpdata = this.chartlist.ctp;
          this.ltpdata = this.data.ltpdata = this.chartlist.ltp;
          this.lowprice = this.data.lowprice = this.chartlist.lowPrice;
          this.highprice = this.data.highprice = this.chartlist.highPrice;
          this.act = this.data.ACTION = this.chartlist.action;
          this.data.rocdata = this.chartlist.roc;
          if (this.data.rocdata > 0) {
            this.data.rocreact = true;
          }
          else {
            this.data.rocreact = false;
          }
          if (this.data.rocdata < 0) {
            this.data.negetive = true;
          }
          else {
            this.data.negetive = false;
          }
          this.data.volumndata =parseFloat(this.chartlist.volume);
          if (this.data.rocdata >= 0) { this.rocreact = true }
          if (this.data.act == 'sell') {
            this.data.react = true;
          }
          else {
            this.data.react = false;
          }
        }
        else{
          this.chartlist = result[0];
          this.ctpdata = this.data.ctpdata = this.chartlist.ctp;
          this.ltpdata = this.data.ltpdata = this.chartlist.ltp;
          this.lowprice = this.data.lowprice = this.chartlist.lowPrice;
          this.highprice = this.data.highprice = this.chartlist.highPrice;
          this.act = this.data.ACTION = this.chartlist.action;
          this.data.rocdata = this.chartlist.roc;
          if (this.data.rocdata > 0) {
            this.rocreact = true;
          }
          else {
            this.data.rocreact = false;
          }
          if (this.data.rocdata < 0) {
            this.data.negetive = true;
          }
          else {
            this.data.negetive = false;
          }
          // this.data.volumndata = this.chartlist.VOLUME;
          if (this.data.rocdata >= 0) { this.rocreact = true }
          if (this.data.act == 'sell') {
            this.data.react = true;
          }
          else {
            this.data.react = false;
          }
        }
      }
      else{
            this.ctpdata = this.ctpdata = '0';
            this.ltpdata=this.data.ltpdata = '0';
            this.lowprice = this.data.lowprice= '0';
            this.highprice=this.data.highprice = '0';
            this.rocdata=this.data.ACTION =0;
            this.volumndata=this.data.volumndata = 0; 
      }
    
     } 
    if (this.token == null) {
      this.route.navigateByUrl('/login');
    }
    this.userName = localStorage.getItem('user_name');
    this.userid = localStorage.getItem('user_id');
    this.act = localStorage.getItem('access_token');
    this.imageLink = (this.profilePic) ? this.data.WEBSERVICE + '/user/' + this.userid + '/file/' + this.profilePic + '?access_token=' + this.act : './assets/img/default.png';
    this.loader = false;
    
  }
 
  // ngOnchanges(){
  //   //  this._orderbook.serverSentEventForOrderbookAsk();
  //   //  this.trade.GetTradeDetail();
  //   // this.trade.getOfferLists();
  //   // this.getBuyVal(event);
  //   // this.getSellVal(event);
  //   // this.getNewCurrency();
  // }
  Selectedlangulage(lang) {
    localStorage.getItem('selectedlang');
  }

  open(content) {
    this.modalService.open(content);
  }

  changebg(val) {
    this.screencolor = val;
    this.data.changescreencolor = val;
    if (this.data.changescreencolor == true) {
      $(".content-wrapper").css("background-color", "#ececec").addClass("intro");
      document.getElementById("night").style.display = "block";
      document.getElementById("light").style.display = "none";

    } else {
      $(".content-wrapper").css("background-color", "#060707").removeClass("intro");
      document.getElementById("light").style.display = "block";
      document.getElementById("night").style.display = "none";
      document.getElementById("drag-btn").style.backgroundColor = "#292931";
    }
  }
  getNewCurrency() {
 this.currencyapi=this.http.get<any>('https://api.tomya.com/CacheService/api/getData?Name=Assets')
      .subscribe(responseCurrency => {
        this.filterCurrency = JSON.parse(responseCurrency.value);
        var mainArr = [];
        this.header = this.filterCurrency.Header;
        this.assets = this.filterCurrency.Values;
      }
      )
  }
  buySellCode(item) {
    this.currency_code = item.currencyCode;
    this.base_currency = item.baseCurrency;
    this.data.selectedSellingAssetText = this.base_currency;
    this.data.selectedBuyingAssetText = this.currency_code;
    // this.data.cur =this.data.selectedBuyingAssetText === "TRY"
    // ? "$"
    // : this.data.selectedSellingAssetText + " ";
   this.data.selectedBuyingCryptoCurrencyName =this.base_currency+ this.currency_code;
    this.data.selectedSellingCryptoCurrencyName = this.currency_code + this.base_currency;
    this.selectedBuyingAssetText = item.currencyCode;
    this.selectedSellingAssetText = item.baseCurrency;
    localStorage.setItem("buying_crypto_asset", this.currency_code.toLocaleLowerCase());
    localStorage.setItem("selling_crypto_asset", this.base_currency.toLocaleLowerCase());
    this.data.cur = this.selectedSellingAssetText;
    this.assetpairsell = item.currencyCode + item.baseCurrency;
    this.assetpairbuy = item.baseCurrency + item.currencyCode;
    this.data.selectedSellingCryptoCurrencyissuer = "";
    this.data.selectedBuyingCryptoCurrencyissuer = "";
         this.stoploss.update();
        // this.stoploss.market = false;
        this._orderbook.serverSentEventForOrderbookAsk();
       // this.getMarketTradeBuy();
        this.trade.reload();
        this.stoploss.onlyBuyAmount = this.stoploss.onlyBuyPrice = this.stoploss.onlyBuyTotalPrice = 0;
        this.stoploss.onlySellAmount = this.stoploss.onlySellPrice = this.stoploss.onlySellTotalPrice = 0;
        $('.reset').click();
        this.trade.myTradeDisplay(0);
        $('#trade').click();
        $('#dropHolder').css('overflow', 'scroll');
        $(window).resize(function () {
          var wd = $('#chartHolder').width();
          $('#dropHolder').width(wd);
          $('#dropHolder').css('overflow', 'scroll');
        });
     
        var url='https://api.tomya.com/StreamingApiTest/rest/trendsTradeGraphFor24Hours/' + this.currency_code + '/' + this.base_currency;
        if (this.data.source6 != undefined ) {
          this.data.source6.close();
        }
        if (this.data.source5 != undefined ) {
          this.data.source5.close();
        }
        if (this.data.source4 != undefined ) {
          this.data.source4.close();
        }
      this.data.source6 = new EventSource(url);
      this.urlAsk = url;
      var result: any = new Object();
      this.data.source6.onmessage = (event: MessageEvent) => {
        result = event.data;
        result = JSON.parse(event.data);
        if (result.length !=0) {
          
          if(this.currency_code == 'BTC' || this.base_currency == 'TRY' || this.base_currency == 'USDT' || this.base_currency == 'BTC'){
            this.chartlist = result[0];
            this.ctpdata = this.data.ctpdata = this.chartlist.ctp;
            this.ltpdata = this.data.ltpdata = this.chartlist.ltp;
            this.lowprice = this.data.lowprice = this.chartlist.lowPrice;
            this.highprice = this.data.highprice = this.chartlist.highPrice;
            this.act = this.data.ACTION = this.chartlist.action;
            this.data.rocdata = parseFloat(this.chartlist.roc).toFixed(4);
            if (this.data.rocdata > 0) {
              this.rocreact = true;
            }
            else {
              this.data.rocreact = false;
            }
            if (this.data.rocdata < 0) {
              this.data.negetive = true;
            }
            else {
              this.data.negetive = false;
            }
            this.data.volumndata =parseFloat(this.chartlist.volume).toFixed(2);
            if (this.data.rocdata >= 0) { this.rocreact = true }
            if (this.data.act == 'sell') {
              this.data.react = true;
            }
            else {
              this.data.react = false;
            }
          }
          else{
            this.chartlist = result[0];
            this.ctpdata = this.data.ctpdata = this.chartlist.ctp;
            this.ltpdata = this.data.ltpdata = this.chartlist.ltp;
            this.lowprice = this.data.lowprice = this.chartlist.lowPrice;
            this.highprice = this.data.highprice = this.chartlist.highPrice;
            this.act = this.data.ACTION = this.chartlist.action;
            this.data.rocdata = this.chartlist.roc;
            if (this.data.rocdata > 0) {
              this.data.rocreact = true;
            }
            else {
              this.data.rocreact = false;
            }
            if (this.data.rocdata < 0) {
              this.data.negetive = true;
            }
            else {
              this.data.negetive = false;
            }
             this.data.volumndata = this.chartlist.volume;
            // if (this.data.rocdata >= 0) { this.rocreact = true }
            if (this.data.act == 'sell') {
              this.data.react = true;
            }
            else {
              this.data.react = false;
            }
          }
        }
        else{
          this.data.ctpdata = '0';
          this.data.ltpdata = '0';
           this.data.lowprice= '0';
            this.data.highprice = '0';
              this.data.rocdata ='0';
              this.data.volumndata = 0; 
        }
      
       } 

  }
  // randomNoForOrderBook(minVal: any, maxVal: any): number {
  //   var minVal1: number = parseInt(minVal);
  //   var maxVal1: number = parseInt(maxVal);
  //   return Math.floor(Math.random() * (maxVal1 - minVal1 + 2) + minVal1);
  // }
  
  ngDoCheck() {
    this.changemode();
  }

  changemode() {
    if (this.data.changescreencolor == true) {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#fefefe");
      $(".bg-black").css("background-color", "transparent");
      $(".btn").css("color", "#000");
      $(".text-blue").css("color", "black");
    } else {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#16181a");
      $(".bg-black").css("background-color", "transparent");
      $(".btn").css("color", "#fff");
      $(".text-blue").css("color", "white");
    }
  }

  // GetTradeDetail() {
  //   var tradeObj = {};
  //   tradeObj["userId"] = localStorage.getItem("user_id");
  //   tradeObj["page_no"] = 1;
  //   tradeObj["no_of_items_per_page"] = 10;
  //   tradeObj[
  //     "buying_asset_code"
  //   ] = this.selectedBuyingAssetText.toLocaleLowerCase();
  //   tradeObj[
  //     "selling_asset_code"
  //   ] = this.selectedSellingAssetText.toLocaleLowerCase();
  //   var jsonString = JSON.stringify(tradeObj);
  //  this.tradeapi= this.http
  //     .post<any>(
  //       this.data.WEBSERVICE + "/userTrade/GetTradeHistoryById",
  //       jsonString,
  //       {
  //         headers: {
  //           "Content-Type": "application/json",
  //           authorization: "BEARER " + localStorage.getItem("access_token")
  //         }
  //       }
  //     )
  //     .subscribe(
  //       response => {
  //         var result = response;
  //         if (result.error.error_data != 0) {
  //         } else {
  //           var tradeDetails = result.tradeListResult;
  //           this.historyTableTr = "";
  //           if (tradeDetails)
  //             if (tradeDetails.length > 0) {
  //               for (var i = 0; i < tradeDetails.length; i++) {
  //                 var action = tradeDetails[i].action;
  //                 var trnid = tradeDetails[i].trade_id;
  //                 var timestamp = tradeDetails[i].transactionTimeStamp;
  //                 var timestampArr = timestamp.split(".");
  //                 timestamp = this.data.readable_timestamp(timestampArr[0]);
  //                 this.historyTableTr +=
  //                   '<tr class="filter ' + action.toLowerCase() + '">';
  //                 this.historyTableTr += "<td>" + timestamp + "</td>";
  //                 this.historyTableTr +=
  //                   '<td class="">' +
  //                   tradeDetails[i].buying_asset_code.toUpperCase() +
  //                   "</td>";
  //                 this.historyTableTr +=
  //                   '<td class="">' +
  //                   tradeDetails[i].selling_asset_code.toUpperCase() +
  //                   "</td>";
  //                 this.historyTableTr +=
  //                   '<td class="">' +
  //                   parseFloat(tradeDetails[i].amount) +
  //                   "&nbsp;" +
  //                   tradeDetails[i].buying_asset_code.toUpperCase() +
  //                   "</td>";
  //                 this.historyTableTr +=
  //                   '<td class="">' +
  //                   parseFloat(tradeDetails[i].price).toFixed(6) +
  //                   "</td>";
  //                 var cls = action == "BUY" ? "skyblue" : "red";
  //                 this.historyTableTr +=
  //                   '<td class="text-' + cls + '">' + action + "</td>";
  //                 this.historyTableTr += "</tr>";
  //               }
  //             } else {
  //               this.historyTableTr +=
  //                 '<tr><td colspan="5" class="text-center">No Data Available</td></tr>';
  //             }
  //           $("#myTradeBody").html(this.historyTableTr);
  //           if (this.myTradeTableDisplayStatus == 0) {
  //             $("#myTradeBody")
  //               .children(".filter")
  //               .show();
  //           }
  //           if (this.myTradeTableDisplayStatus == 1) {
  //             $("#myTradeBody")
  //               .children(".filter")
  //               .show();
  //             $("#myTradeBody")
  //               .children("tr.sell")
  //               .hide();
  //           }
  //           if (this.myTradeTableDisplayStatus == 2) {
  //             $("#myTradeBody")
  //               .children(".filter")
  //               .show();
  //             $("#myTradeBody")
  //               .children("tr.buy")
  //               .hide();
  //           }
  //         }
  //       },
  //       reason => {
  //         if (reason.error.error == "invalid_token") {
  //           this.data.logout();
  //           this.data.alert("Session Timeout. Login Again", "warning");
  //         } else this.data.alert("Could Not Connect To Server", "danger");
  //       }
  //     );
  // }

  // myTradeDisplay(value) {
  //   this.myTradeTableDisplayStatus = value;
  //   this.trade.GetTradeDetail();
  // }
  // getOfferLists() {
  //   this.selectedBuyingAssetText = this.data.selectedBuyingAssetText.toLowerCase();
  //   this.selectedSellingAssetText = this.data.selectedSellingAssetText.toLowerCase();
  //   var inputObj = {};
  //   var mnObj;
  //   inputObj["userId"] = localStorage.getItem("user_id");
  //   inputObj[
  //     "selling_asset_code"
  //   ] = this.data.selectedBuyingAssetText.toUpperCase();
  //   inputObj[
  //     "buying_asset_code"
  //   ] = this.data.selectedSellingAssetText.toUpperCase();
  //   var jsonString = JSON.stringify(inputObj);
  //   if (localStorage.getItem("user_id") != null)
  //   this.offerlistapi= this.http
  //       .post<any>(
  //         this.data.WEBSERVICE + "/userTrade/GetOfferByAccountID",
  //         jsonString,
  //         { headers: { "Content-Type": "application/json" } }
  //       )
  //       .subscribe(
  //         response => {
  //           var result = response;
  //           if (result.error.error_data != "0") {
  //             this.data.alert(result.error.error_msg, "danger");
  //           } else {
  //             var result = response;
  //             var myOfferRecords = result.tradeListResult;
  //             mnObj = [];
  //             if (myOfferRecords)
  //               for (var i = 0; i < myOfferRecords.length; i++) {
  //                 if (
  //                   (myOfferRecords[i].buying_asset_code ==
  //                     this.selectedBuyingAssetText.toUpperCase() &&
  //                     myOfferRecords[i].selling_asset_code ==
  //                       this.selectedSellingAssetText.toUpperCase()) ||
  //                   (myOfferRecords[i].buying_asset_code ==
  //                     this.selectedSellingAssetText.toUpperCase() &&
  //                     myOfferRecords[i].selling_asset_code ==
  //                       this.selectedBuyingAssetText.toUpperCase())
  //                 ) {
  //                   if (myOfferRecords[i].txn_type == 1) {
  //                     var myOfferType = "Buy";
  //                   }
  //                   if (myOfferRecords[i].txn_type == 2) {
  //                     var myOfferType = "Sell";
  //                   }
  //                   var rsObj = {};
  //                   var timestamp = myOfferRecords[i].transactionTimeStamp;
  //                   var timestampArr = timestamp.split(".");
  //                   rsObj["time"] = this.data.readable_timestamp(
  //                     timestampArr[0]
  //                   );
  //                   rsObj["offerId"] = myOfferRecords[i].offer_id;
  //                   rsObj["amount"] = parseFloat(myOfferRecords[i].amount);
  //                   rsObj["buy"] = myOfferRecords[i].buying_asset_code;
  //                   if (this.selectedSellingAssetText == "usd") {
  //                     rsObj["price"] = parseFloat(myOfferRecords[i].price);
  //                     rsObj["volume"] = (
  //                       parseFloat(myOfferRecords[i].price) *
  //                       parseFloat(myOfferRecords[i].amount)
  //                     ).toFixed(8);
  //                   } else {
  //                     rsObj["price"] = parseFloat(myOfferRecords[i].price);
  //                     rsObj["volume"] = (
  //                       parseFloat(myOfferRecords[i].price) *
  //                       parseFloat(myOfferRecords[i].amount)
  //                     ).toFixed(8);
  //                   }
  //                   rsObj["type"] = myOfferType;
  //                   rsObj["txn_typ"] = myOfferRecords[i].txn_type;
  //                   mnObj.push(rsObj);
  //                 }
  //               }
  //             this.myOfferRecordsHtml = mnObj;
  //           }
  //         },
  //         reason => {
  //           if (reason.error.error == "invalid_token") {
  //             this.data.logout();
  //             this.data.alert("Session Timeout. Login Again", "warning");
  //           } else {
  //             this.data.logout();
  //             this.data.alert("Could Not Connect To Server", "danger");
  //           }
  //         }
  //       );
  // }

  getOfferDetails(content, offerId) {
    var inputObj = {};
    this.data.alert("Loading...", "dark");
    inputObj["offer_id"] = offerId;
    inputObj["userId"] = localStorage.getItem("user_id");
    var jsonString = JSON.stringify(inputObj);
  this.offerdtlapi= this.http
      .post<any>(this.data.WEBSERVICE + "/userTrade/GetOfferByID", jsonString, {
        headers: { "Content-Type": "application/json" }
      })
      .subscribe(
        response => {
          this.data.loader = false;
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            this.manageOfferId = offerId;
            this.modifyAmount = parseFloat(result.tradeResult.amount);
            this.modifyPrice = parseFloat(result.tradeResult.price);
            this.modifyVolume =
              parseFloat(this.modifyAmount) * parseFloat(this.modifyPrice);
            this.modifyType = result.tradeResult.txn_type;
            this.modalService.open(content, { centered: true });
          }
        },
        reason => {
          if (reason.error.error == "invalid_token") {
            this.data.logout();
            this.data.alert("Session Timeout. Login Again", "warning");
          } else this.data.alert("Could Not Connect To Server", "danger");
        }
      );
  }

  manageOffer() {
   
    this.data.alert("Loading...", "dark");
    var inputObjPC = {};
    if(this.modifyType==1){
      inputObjPC[
        "selling_asset_code"
      ] = this.data.selectedSellingAssetText.toUpperCase(); // change by sanu
      inputObjPC[
        "buying_asset_code"
      ] = this.data.selectedBuyingAssetText.toUpperCase(); //
      inputObjPC["userId"] = localStorage.getItem("user_id");
      inputObjPC["price"] = this.modifyPrice;
      inputObjPC["txn_type"] =this.modifyType;
    }
    else{
      inputObjPC[
        "selling_asset_code"
      ] =this.data.selectedBuyingAssetText.toUpperCase(); // change by sanu
      inputObjPC[
        "buying_asset_code"
      ] = this.data.selectedSellingAssetText.toUpperCase(); //
      inputObjPC["userId"] = localStorage.getItem("user_id");
      inputObjPC["price"] = this.modifyPrice;
      inputObjPC["txn_type"] =this.modifyType;
    }
   
    var jsonString = JSON.stringify(inputObjPC);
   this.checkoffer= this.http
    .post<any>(
      this.data.WEBSERVICE + "/userTrade/OfferPriceCheck",
      jsonString,
      {
        headers: {
          "Content-Type": "application/json"
        }
      }
    )
    .subscribe(response => {

      var result = response;
      if (result.error.error_data != "0") {
        this.data.alert(result.error.error_msg, "warning");
        $(".tradeBtn").attr("disabled", true);
      } else {

     var inputObj = {};
      inputObj["offer_id"] = this.manageOfferId;
      inputObj["userId"] = localStorage.getItem("user_id");
      if (this.modifyType == 1) {
        inputObj["selling_asset_code"] = this.data.selectedSellingAssetText.toUpperCase();
        inputObj["buying_asset_code"] = this.data.selectedBuyingAssetText.toUpperCase();
      } else {
        inputObj["buying_asset_code"] = this.data.selectedSellingAssetText.toUpperCase();
        inputObj["selling_asset_code"] = this.data.selectedBuyingAssetText.toUpperCase();
      }
      inputObj["amount"] = this.modifyAmount;
      inputObj["price"] = this.modifyPrice;
      inputObj["txn_type"] = this.modifyType;
  
      var jsonString = JSON.stringify(inputObj);
    this.trademanagerapi=this.http
      .post<any>(
        this.data.WEBSERVICE + "/userTrade/TradeManageOffer",
        jsonString,
        {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        }
      )
      .subscribe(
        response => {
          var result = response;
          this.data.loader = false;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            $(".reset").click();
            this.data.alert(result.error.error_msg, "success");
            this.trade.GetTradeDetail();
            this.trade.getOfferLists();

            $("#trade").click();
          }
        },
        reason => {
          if (reason.error.error == "invalid_token") {
            this.data.logout();
            this.data.alert("Session Timeout. Login Again", "warning");
          } else this.data.alert("Could Not Connect To Server", "danger");
        }
      );
      }

      })
 
  }

  deleteTrade(content, offerId, txnType, price) {
    this.delOfferId = offerId;
    this.delTxnType = txnType;
    this.delPrice = price;
    this.modalService.open(content, { centered: true });
  }

  delOffer() {
    this.data.alert("Loading...", "dark");
    $(".load").fadeIn();
    var inputObj = {};
    inputObj["offer_id"] = this.delOfferId;
    inputObj["userId"] = localStorage.getItem("user_id");
    if (this.delTxnType == 2) {
      inputObj[
        "selling_asset_code"
      ] = this.selectedBuyingAssetText.toUpperCase();
      inputObj[
        "buying_asset_code"
      ] = this.selectedSellingAssetText.toUpperCase();
    }
    if (this.delTxnType == 1) {
      inputObj[
        "selling_asset_code"
      ] = this.selectedSellingAssetText.toUpperCase();
      inputObj[
        "buying_asset_code"
      ] = this.selectedBuyingAssetText.toUpperCase();
    }
    inputObj["amount"] = "0";
    inputObj["txn_type"] = this.delTxnType;
    inputObj["price"] = this.delPrice;
    var jsonString = JSON.stringify(inputObj);
  this.deleteoffer=  this.http
      .post<any>(
        this.data.WEBSERVICE + "/userTrade/TradeManageOffer",
        jsonString,
        {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        }
      )
      .subscribe(
        response => {
          this.data.loader = false;
          $(".load").fadeOut();
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "warning");
          } else {
            this.trade.GetTradeDetail();
            this.stoploss.getUserTransaction();
            $(".reset").click();
            this.data.alert(result.error.error_msg, "success");
            $("#trade").click();
          }
        },
        reason => {
          if (reason.error.error == "invalid_token") {
            this.data.logout();
            this.data.alert("Session Timeout. Login Again", "warning");
          } else this.data.alert("Could Not Connect To Server", "danger");
        }
      );
  }

  reset() {
    this.limitPrice = "";
    this.limitValue = "";
    this.limitPrice = "";
    this.onlyBuyAmount = this.onlyBuyPrice = this.onlyBuyTotalPrice = "";
    this.onlySellAmount = this.onlySellPrice = this.onlySellTotalPrice = "";
    $(function() {
      $("input.form-control").val("");
    });
  }

  // update() {
  //   this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
  //   this.selectedSellingAssetText = this.data.selectedSellingAssetText;
  //   this.market = true;
  // }

  getBuyVal(event) {
    var val = event.target.value;
    if (val < 0) {
      this.data.alert("Price cannot be negative", "warning");
      this.onlyBuyAmount = "";
    } else {
      var onlyBuyAmount: any = val;
    }
   this.byvalapi= this.http
      .get<any>(
        this.data.TRADESERVICE +
          "/getAmountBuy/" +
          this.data.selectedSellingAssetText.toUpperCase() +
          this.data.selectedBuyingAssetText.toUpperCase() +
          "/" +
          this.data.selectedBuyingAssetText.toUpperCase() +
          this.data.selectedSellingAssetText.toUpperCase() +
          "/" +
          onlyBuyAmount
      )
      .subscribe(
        data => {
          var result = data;
          if (result.code == "0") {
            if (this.data.selectedSellingAssetText == "USDT") {
              this.onlyBuyPrice = parseFloat(result.price).toFixed(4);
              this.onlyBuyTotalPrice = (
                parseFloat(result.price) * parseFloat(onlyBuyAmount)
              ).toFixed(4);
            } else {
              this.onlyBuyPrice = parseFloat(result.price).toFixed(7);
              this.onlyBuyTotalPrice = (
                parseFloat(result.price) * parseFloat(onlyBuyAmount)
              ).toFixed(7);
            }
            $(".onlyBuyError").hide();
          } else {
            this.onlyBuyPrice = 0;
            this.onlyBuyTotalPrice = 0;
            $(".onlyBuyError").show();
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  marketBuy() {
    this.data.alert("Loading...", "dark");
    var onlyBuyAmount = this.onlyBuyAmount;
  this.marketbuyapi= this.http
      .get<any>(
        this.data.TRADESERVICE +
          "/getAmountBuy/" +
          this.data.selectedSellingAssetText.toUpperCase() +
          this.data.selectedBuyingAssetText.toUpperCase() +
          "/" +
          this.data.selectedBuyingAssetText.toUpperCase() +
          this.data.selectedSellingAssetText.toUpperCase() +
          "/" +
          onlyBuyAmount
      )
      .subscribe(data => {
        var result = data;
        if (result.code == "0") {
          if (this.data.selectedSellingAssetText == "USDT" || this.data.selectedSellingAssetText == "TRY") {
            this.onlyBuyPrice = parseFloat(result.price).toFixed(4);
            this.onlyBuyPrice1 = parseFloat(result.price1);
          } else {
            this.onlyBuyPrice = parseFloat(result.price).toFixed(7);
            this.onlyBuyPrice1 = parseFloat(result.price1);
          }
          $(".onlyBuyError").hide();
          var inputObj = {};
          inputObj[
            "selling_asset_code"
          ] = this.data.selectedSellingAssetText.toUpperCase(); // change by sanu
          inputObj[
            "buying_asset_code"
          ] = this.data.selectedBuyingAssetText.toUpperCase(); //
          inputObj["userId"] = localStorage.getItem("user_id");
          inputObj["price"] = this.onlyBuyPrice1;
          inputObj["txn_type"] = "1";
          var jsonString = JSON.stringify(inputObj);
        this.offercheckapi= this.http
            .post<any>(
              this.data.WEBSERVICE + "/userTrade/OfferPriceCheck",
              jsonString,
              {
                headers: {
                  "Content-Type": "application/json"
                }
              }
            )
            .subscribe(response => {
              var result = response;
              if (result.error.error_data != "0") {
                this.data.alert(result.error.error_msg, "warning");
                $(".tradeBtn").attr("disabled", true);
              } else {
                var inputObj = {};
                inputObj["userId"] = localStorage.getItem("user_id");
                inputObj[
                  "selling_asset_code"
                ] = this.data.selectedSellingAssetText.toUpperCase();
                inputObj[
                  "buying_asset_code"
                ] = this.data.selectedBuyingAssetText.toUpperCase();
                inputObj["amount"] = parseFloat(this.onlyBuyAmount);
                inputObj["price"] = this.onlyBuyPrice1;
                inputObj["offerType"] ='M';
                inputObj["txn_type"] = "1";
                var jsonString = JSON.stringify(inputObj);
                if (this.onlyBuyPrice1 * this.onlyBuyAmount >= 0.00001) {
                this.createoffer= this.http
                    .post<any>(
                      this.data.WEBSERVICE + "/userTrade/TradeCreateOffer",
                      jsonString,
                      {
                        headers: {
                          "Content-Type": "application/json",
                          authorization:
                            "BEARER " + localStorage.getItem("access_token")
                        }
                      }
                    )

                    .subscribe(data => {
                      this.data.loader = false;
                      var result = data;
                      if (result.error.error_data != "0") {
                        //if (result.error.error_data == 1)
                          this.data.alert(result.error.error_msg, "danger");
                        //  $("#warn").click();
                      } else {
                        this.reset();
                        this.data.alert(result.error.error_msg, "success");
                      }
                      this.reset();
                    },
                      function(response) {
                        // wip(0);
                        if (response.error.error == "invalid_token") {
                          this.data.alert("Could Not Connect To Server", "danger");
                          this.data.logout();
                        } else {
                          this.data.logout();
                          this.data.alert("Could Not Connect To Server", "danger");
                        }
                      }
                    );
                } else {
                  this.reset();
                  this.data.loader = false;
                  this.data.alert(
                    "Offer Value is lesser than permissible value",
                    "warning"
                  );
                }
              }
            });
        } else {
          this.onlyBuyAmount = 0;
          $(".onlyBuyError").show();
        }
      });
  }

  getSellVal(event) {
    var val = event.target.value;
    if (val < 0) {
      this.data.alert("Price cannot be negative", "warning");
      this.onlySellAmount = "";
    } else {
      var onlySellAmount: any = val;
    }
    this.getamountsell=this.http
      .get<any>(
        this.data.TRADESERVICE +
          "/getAmountSell/" +
          this.data.selectedBuyingAssetText.toUpperCase() +
          this.data.selectedSellingAssetText.toUpperCase() +
          "/" +
          this.data.selectedSellingAssetText.toUpperCase() +
          this.data.selectedBuyingAssetText.toUpperCase() +
          "/" +
          onlySellAmount
      )
      .subscribe(data => {
        var result = data;
        if (result.code == "0") {
          if (this.data.selectedSellingAssetText == "usd") {
            this.onlySellPrice = parseFloat(result.price).toFixed(4);
            this.onlySellTotalPrice = (
              parseFloat(result.price) * parseFloat(onlySellAmount)
            ).toFixed(4);
          } else {
            this.onlySellPrice = parseFloat(result.price).toFixed(7);
            this.onlySellTotalPrice = (
              parseFloat(result.price) * parseFloat(onlySellAmount)
            ).toFixed(7);
          }
          $(".onlySellError").hide();
        } else {
          this.onlySellPrice = 0;
          this.onlySellTotalPrice = 0;
          $(".onlySellError").show();
        }
      });
  }

  marketSell() {
    this.data.alert("Loading...", "dark");
    $(".load").fadeIn();
    $("#msell").attr("disabled", true);
    var onlyBuyAmount = this.onlySellAmount;
   this.geTamountSellapi= this.http
      .get<any>(
        this.data.TRADESERVICE +
          "/getAmountSell/" +
          this.data.selectedBuyingAssetText.toUpperCase() +
          this.data.selectedSellingAssetText.toUpperCase() +
          "/" +
          this.data.selectedSellingAssetText.toUpperCase() +
          this.data.selectedBuyingAssetText.toUpperCase() +
          "/" +
          onlyBuyAmount
      )
      .subscribe(data => {
        var result = data;
        if (result.code == "0") {
          if (this.data.selectedSellingAssetText == "USDT" || this.data.selectedSellingAssetText == "TRY") {
            this.onlySellPrice = parseFloat(result.price).toFixed(4);
            this.onlySellPrice1 = parseFloat(result.price1);
          } else {
            this.onlySellPrice = parseFloat(result.price).toFixed(7);
            this.onlySellPrice1 = parseFloat(result.price1);
          }
          $(".onlySellError").hide();
          var inputObj = {};
          inputObj[
            "selling_asset_code"
          ] = this.data.selectedBuyingAssetText.toUpperCase(); //
          inputObj[
            "buying_asset_code"
          ] = this.data.selectedSellingAssetText.toUpperCase(); //change by sanu
          inputObj["userId"] = localStorage.getItem("user_id");
          inputObj["price"] = this.onlySellPrice1;
          inputObj["txn_type"] = "2";
          var jsonString = JSON.stringify(inputObj);
         this.Offerpricecheckapi= this.http
            .post<any>(
              this.data.WEBSERVICE + "/userTrade/OfferPriceCheck",
              jsonString,
              {
                headers: {
                  "Content-Type": "application/json"
                }
              }
            )
            .subscribe(response => {
              var result = response;
              if (result.error.error_data != "0") {
                this.data.alert(result.error.error_msg, "danger");
               // $("#warn").click();
                $(".tradeBtn").attr("disabled", true);
                // if (result.error.error_data == 1)
                //
                // else $("#warn").click();
                //
              } else {
                var inputObj = {};
                inputObj["userId"] = localStorage.getItem("user_id");
                inputObj[
                  "selling_asset_code"
                ] = this.data.selectedBuyingAssetText.toUpperCase();
                inputObj[
                  "buying_asset_code"
                ] = this.data.selectedSellingAssetText.toUpperCase();
                inputObj["amount"] = parseFloat(this.onlySellAmount);
                inputObj["price"] = parseFloat(this.onlySellPrice1);
                inputObj["offerType"] ='M';
                inputObj["txn_type"] = "2";
                var jsonString = JSON.stringify(inputObj);
                if (this.onlySellPrice1 * this.onlySellAmount >= 0.00001) {
                  this.tradecreateOffer=this.http
                    .post<any>(
                      this.data.WEBSERVICE + "/userTrade/TradeCreateOffer",
                      jsonString,
                      {
                        headers: {
                          "Content-Type": "application/json",
                          authorization:
                            "BEARER " + localStorage.getItem("access_token")
                        }
                      }
                    )
                    .subscribe(data => {
                      this.data.loader = false;
                      $(".load").fadeOut();
                      var result = data;
                      if (result.error.error_data != "0") {
                        // if (result.error.error_data == 1)
                           this.data.alert(result.error.error_msg, "danger");
                          // $("#warn").click();
                        // else $("#warn").click();

                      } else {
                        this.reset();
                        this.data.alert(result.error.error_msg, "success");
                      }
                    },
                      function(response) {
                        // wip(0);
                        if (response.error.error == "invalid_token") {
                          this.data.alert("Could Not Connect To Server", "danger");
                          this.data.logout();
                        } else {
                          this.data.logout();
                          this.data.alert("Could Not Connect To Server", "danger");
                        }
                      }
                    );
                } else {
                  this.reset();
                  this.data.loader = false;
                  this.data.alert(
                    "Offer Value is lesser than permissible value",
                    "warning"
                  );
                }
              }
            });
        } else {
          this.onlySellPrice = 0;
          $(".onlySellError").show();
        }
      });
  }
  
  ngOnDestroy(){
    if(this.data.source5 !=undefined){
      this.data.source5.close();   
    }
    if(this.data.source6 !=undefined){
      this.data.source6.close();   
    }
  if(this.currencyapi !=undefined){
  
    this.currencyapi.unsubscribe();
   
  }
  if(this.tradeapi !=undefined){
    this.tradeapi.unsubscribe();
  }
  if(this.offerlistapi!=undefined){
    this.offerlistapi.unsubscribe();
  }
  if(this.offerdtlapi!=undefined){
    this.offerdtlapi.unsubscribe();
  }
  }

}
