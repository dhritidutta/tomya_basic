import { Component, OnInit, DoCheck } from "@angular/core";
import { CoreDataService } from "../core-data.service";
import { HttpClient } from "@angular/common/http";
import * as $ from "jquery";
import { BodyService } from "../body.service";
import { DashboardComponent } from "../dashboard/dashboard.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MyWalletComponent } from "../my-wallet/my-wallet.component";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";
@Component({
  selector: "app-stop-loss",
  templateUrl: "./stop-loss.component.html",
  styleUrls: ["./stop-loss.component.css"]
})
export class StopLossComponent implements DoCheck {
  selectedBuyingAssetText: string;
  selectedSellingAssetText: string;
  market: boolean;
  onlyBuyAmount: any;
  onlyBuyPrice: any;
  onlyBuyTotalPrice: any;
  onlySellAmount: any;
  onlySellPrice: any;
  onlySellTotalPrice: any;
  mode: any;
  modeMessage: any;
  buyPriceText: string;
  sellPriceText: string;
  fiatBalance: number;
  fiatBalanceText: string;
  sellPrice: string;
  totalFiatBalance: any;
  fiatBalanceLabel: string;
  btcBalance: string;
  bchBalance: any;
  hcxBalance: string;
  iecBalance: string;
  buyPrice: any;
  btcBalanceInUsd: string;
  bchBalanceInUsd: string;
  hcxBalanceInUsd: string;
  iecBalanceInUsd: string;
  selectedCryptoCurrency: string;
  selectedCryptoCurrencyBuy: string;
  selectedCryptoCurrencySell: string;
  selectedCryptoCurrencyBalance: string;
  selelectedBuyingAssetBalance: string = "0";
  selelectedSellingAssetBalance: string = "0";
  btcBought: any;
  btcSold: any;
  bchBought: any;
  bchSold: any;
  hcxBought: any;
  hcxSold: any;
  iecBought: any;
  iecSold: any;
  marketOrderPrice: number;
  stopLossError: string;
  rateControl: any;
  valLimit: number;
  result: any;
  base_currency: any;
  valid;
  asset;
  currencyBalance;
  getlang: any;
  balencelist;
  assetbalance;
  limitAmount: any = 0;
  limitPrice: any = 0;
  limitValue: any = 0;
  stopLossPrice: any;
  stopLossTriggerPrice: any;
  stopLossQuantity: any;
  onlySellPrice1: any;
  onlyBuyPrice1: any;
  private currencyapi: Subscription;
  private amountby: Subscription;
  private getmntbuy: Subscription;
  private getamntsell: Subscription;
  private sellStoplossApi: Subscription;
  private buystoplossApi: Subscription;
  private stplossbysellApi: Subscription;
  private OfferpricecheckApi: Subscription;
  private tradecreateApi: Subscription;
  private marketsellApi: Subscription;
  private limitbuytradeOffer: Subscription;
  private marketsellOfferpricecheck: Subscription;
  private limitbuyOfferpriceApi: Subscription;
  private limitsellOfferpricecheck: Subscription;
  private limitselltradecreateoffer: Subscription;
  private getusetranApi: Subscription;
  constructor(
    public data: CoreDataService,
    private http: HttpClient,
    public main: BodyService,
    public dash: DashboardComponent,
    public translate: TranslateService,
    private modalService: NgbModal,
    public mywallet: MyWalletComponent
  ) {
    $(function () {
      $(".form-control").click(function () {
        $(this).select();
      });
    });

    this.valLimit = 0;
    translate.addLangs(["Turkish", "English"]);
    this.getlang = localStorage.getItem("selectedlang");
    if (this.getlang == undefined || this.getlang == null) {
      translate.setDefaultLang("Turkish");
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : "Turkish"
      );
    } else {
      translate.setDefaultLang(this.getlang);
      const browserLang = translate.getBrowserLang();

      translate.use(
        browserLang.match(/Turkish|English/) ? browserLang : this.getlang
      );
    }
  }
  ngOnInit() {
 this.getUserTransaction();
   }

  Selectedlangulage(lang) {
    localStorage.getItem("selectedlang");
  }

  ngDoCheck() {
    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
    this.selectedSellingAssetText = this.data.selectedSellingAssetText;
    this.asset = this.selectedSellingAssetText;
    if (this.asset === "TRY") {
      this.valid = true;
    } else {
      this.valid = false;
    }
  }

  reset() {
    this.limitPrice = "";
    this.limitValue = "";
    this.limitPrice = "";
    this.stopLossQuantity = "";
    this.stopLossPrice = "";
    this.stopLossTriggerPrice = "";
    this.onlyBuyAmount = this.onlyBuyPrice = this.onlyBuyTotalPrice = "";
    this.onlySellAmount = this.onlySellPrice = this.onlySellTotalPrice = "";
    $(function () {
      $("input.form-control").val("");
    });
    this.getUserTransaction();
  }

  update() {
    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
    this.selectedSellingAssetText = this.data.selectedSellingAssetText;
    this.market = true;
  }

  getBuyVal(event) {
    var val = event.target.value;
    if (val < 0) {
      this.data.alert("Price cannot be negative", "warning");
      this.onlyBuyAmount = "";
    } else {
      var onlyBuyAmount: any = val;
    }
    this.amountby = this.http
      .get<any>(
        this.data.TRADESERVICE +
        "/getAmountBuy/" +
        this.data.selectedSellingAssetText.toUpperCase() +
        this.data.selectedBuyingAssetText.toUpperCase() +
        "/" +
        this.data.selectedBuyingAssetText.toUpperCase() +
        this.data.selectedSellingAssetText.toUpperCase() +
        "/" +
        onlyBuyAmount
      )
      .subscribe(
        data => {
          var result = data;
          if (result.code == "0") {
            if (this.data.selectedSellingAssetText == "usdt") {
              this.onlyBuyPrice = parseFloat(result.price).toFixed(4);
              this.onlyBuyTotalPrice = (
                parseFloat(result.price) * parseFloat(onlyBuyAmount)
              ).toFixed(4);
            } else {
              this.onlyBuyPrice = parseFloat(result.price).toFixed(7);
              this.onlyBuyTotalPrice = (
                parseFloat(result.price) * parseFloat(onlyBuyAmount)
              ).toFixed(7);
            }
            $(".onlyBuyError").hide();
          } else {
            this.onlyBuyPrice = 0;
            this.onlyBuyTotalPrice = 0;
            $(".onlyBuyError").show();
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  marketBuy() {
    this.data.alert("Loading...", "dark");
    var onlyBuyAmount = this.onlyBuyAmount;
    this.getmntbuy = this.http
      .get<any>(
        this.data.TRADESERVICE +
        "/getAmountBuy/" +
        this.data.selectedSellingAssetText.toUpperCase() +
        this.data.selectedBuyingAssetText.toUpperCase() +
        "/" +
        this.data.selectedBuyingAssetText.toUpperCase() +
        this.data.selectedSellingAssetText.toUpperCase() +
        "/" +
        onlyBuyAmount
      )
      .subscribe(data => {
        var result = data;
        if (result.code == "0") {
          if (this.data.selectedSellingAssetText == "usdt") {
            this.onlyBuyPrice = parseFloat(result.price).toFixed(4);
            this.onlyBuyPrice1 = parseFloat(result.price1);
          } else {
            this.onlyBuyPrice = parseFloat(result.price).toFixed(7);
            this.onlyBuyPrice1 = parseFloat(result.price1);
          }
          $(".onlyBuyError").hide();
          var inputObj = {};
          inputObj[
            "selling_asset_code"
          ] = this.data.selectedSellingAssetText.toUpperCase(); // change by sanu
          inputObj[
            "buying_asset_code"
          ] = this.data.selectedBuyingAssetText.toUpperCase(); //
          inputObj["userId"] = localStorage.getItem("user_id");
          inputObj["price"] = this.onlyBuyPrice1;
          inputObj["txn_type"] = "1";
          var jsonString = JSON.stringify(inputObj);
          this.OfferpricecheckApi = this.http
            .post<any>(
              this.data.WEBSERVICE + "/userTrade/OfferPriceCheck",
              jsonString,
              {
                headers: {
                  "Content-Type": "application/json"
                }
              }
            )
            .subscribe(response => {
              var result = response;
              if (result.error.error_data != "0") {
                this.data.alert(result.error.error_msg, "warning");
                $(".tradeBtn").attr("disabled", true);
              } else {
                var inputObj = {};
                inputObj["userId"] = localStorage.getItem("user_id");
                inputObj[
                  "selling_asset_code"
                ] = this.data.selectedSellingAssetText.toUpperCase();
                inputObj[
                  "buying_asset_code"
                ] = this.data.selectedBuyingAssetText.toUpperCase();
                inputObj["amount"] = parseFloat(this.onlyBuyAmount);
                inputObj["price"] = this.onlyBuyPrice1;
                inputObj["offerType"] = 'M';
                inputObj["txn_type"] = "1";
                var jsonString = JSON.stringify(inputObj);
                if (this.onlyBuyPrice1 * this.onlyBuyAmount >= 0.00001) {
                  this.tradecreateApi = this.http
                    .post<any>(
                      this.data.WEBSERVICE + "/userTrade/TradeCreateOffer",
                      jsonString,
                      {
                        headers: {
                          "Content-Type": "application/json",
                          authorization:
                            "BEARER " + localStorage.getItem("access_token")
                        }
                      }
                    )

                    .subscribe(data => {
                      this.data.loader = false;
                      var result = data;
                      if (result.error.error_data != "0") {
                        //if (result.error.error_data == 1)
                        this.data.alert(result.error.error_msg, "danger");
                        //  $("#warn").click();
                      } else {
                        this.reset();
                        this.data.alert(result.error.error_msg, "success");
                      }
                      this.reset();
                    },
                      function (response) {
                        // wip(0);
                        if (response.error.error == "invalid_token") {
                          this.data.alert("Could Not Connect To Server", "danger");
                          this.data.logout();
                        } else {
                          this.data.logout();
                          this.data.alert("Could Not Connect To Server", "danger");
                        }
                      }
                    );
                } else {
                  this.reset();
                  this.data.loader = false;
                  this.data.alert(
                    "Offer Value is lesser than permissible value",
                    "warning"
                  );
                }
              }
            });
        } else {
          this.onlyBuyAmount = 0;
          $(".onlyBuyError").show();
        }
      });
  }

  getSellVal(event) {
    var val = event.target.value;
    if (val < 0) {
      this.data.alert("Price cannot be negative", "warning");
      this.onlySellAmount = "";
    } else {
      var onlySellAmount: any = val;
    }
    this.getamntsell = this.http
      .get<any>(
        this.data.TRADESERVICE +
        "/getAmountSell/" +
        this.data.selectedBuyingAssetText.toUpperCase() +
        this.data.selectedSellingAssetText.toUpperCase() +
        "/" +
        this.data.selectedSellingAssetText.toUpperCase() +
        this.data.selectedBuyingAssetText.toUpperCase() +
        "/" +
        onlySellAmount
      )
      .subscribe(data => {
        console.log(data);

        var result = data;
        if (result.code == "0") {
          if (this.data.selectedSellingAssetText == "usdt") {
            this.onlySellPrice = parseFloat(result.price).toFixed(4);
            this.onlySellTotalPrice = (
              parseFloat(result.price) * parseFloat(onlySellAmount)
            ).toFixed(4);
          } else {
            this.onlySellPrice = parseFloat(result.price).toFixed(7);
            this.onlySellTotalPrice = (
              parseFloat(result.price) * parseFloat(onlySellAmount)
            ).toFixed(7);
          }
          $(".onlySellError").hide();
        } else {
          this.onlySellPrice = 0;
          this.onlySellTotalPrice = 0;
          $(".onlySellError").show();
        }
      });
  }

  marketSell() {
    this.data.alert("Loading...", "dark");
    $(".load").fadeIn();
    $("#msell").attr("disabled", true);
    var onlyBuyAmount = this.onlySellAmount;
    this.marketsellApi = this.http
      .get<any>(
        this.data.TRADESERVICE +
        "/getAmountSell/" +
        this.data.selectedBuyingAssetText.toUpperCase() +
        this.data.selectedSellingAssetText.toUpperCase() +
        "/" +
        this.data.selectedSellingAssetText.toUpperCase() +
        this.data.selectedBuyingAssetText.toUpperCase() +
        "/" +
        onlyBuyAmount
      )
      .subscribe(data => {
        var result = data;
        if (result.code == "0") {
          if (this.data.selectedSellingAssetText == "usdt") {
            this.onlySellPrice = parseFloat(result.price).toFixed(4);
            this.onlySellPrice1 = parseFloat(result.price1);
          } else {
            this.onlySellPrice = parseFloat(result.price).toFixed(7);
            this.onlySellPrice1 = parseFloat(result.price1);
          }
          $(".onlySellError").hide();
          var inputObj = {};
          inputObj[
            "selling_asset_code"
          ] = this.data.selectedBuyingAssetText.toUpperCase(); //
          inputObj[
            "buying_asset_code"
          ] = this.data.selectedSellingAssetText.toUpperCase(); //change by sanu
          inputObj["userId"] = localStorage.getItem("user_id");
          inputObj["price"] = this.onlySellPrice1;
          inputObj["txn_type"] = "2";
          var jsonString = JSON.stringify(inputObj);
          this.marketsellOfferpricecheck = this.http
            .post<any>(
              this.data.WEBSERVICE + "/userTrade/OfferPriceCheck",
              jsonString,
              {
                headers: {
                  "Content-Type": "application/json"
                }
              }
            )
            .subscribe(response => {
              var result = response;
              if (result.error.error_data != "0") {
                this.data.alert(result.error.error_msg, "danger");
                // $("#warn").click();
                $(".tradeBtn").attr("disabled", true);
                // if (result.error.error_data == 1)
                //
                // else $("#warn").click();
                //
              } else {
                var inputObj = {};
                inputObj["userId"] = localStorage.getItem("user_id");
                inputObj[
                  "selling_asset_code"
                ] = this.data.selectedBuyingAssetText.toUpperCase();
                inputObj[
                  "buying_asset_code"
                ] = this.data.selectedSellingAssetText.toUpperCase();
                inputObj["amount"] = parseFloat(this.onlySellAmount);
                inputObj["price"] = parseFloat(this.onlySellPrice1);
                inputObj["offerType"] = 'M';
                inputObj["txn_type"] = "2";
                var jsonString = JSON.stringify(inputObj);
                if (this.onlySellPrice1 * this.onlySellAmount >= 0.00001) {
                  this.http
                    .post<any>(
                      this.data.WEBSERVICE + "/userTrade/TradeCreateOffer",
                      jsonString,
                      {
                        headers: {
                          "Content-Type": "application/json",
                          authorization:
                            "BEARER " + localStorage.getItem("access_token")
                        }
                      }
                    )
                    .subscribe(data => {
                      this.data.loader = false;
                      $(".load").fadeOut();
                      var result = data;
                      if (result.error.error_data != "0") {
                        // if (result.error.error_data == 1)
                        this.data.alert(result.error.error_msg, "danger");
                        // $("#warn").click();
                        // else $("#warn").click();

                      } else {
                        this.reset();
                        this.data.alert(result.error.error_msg, "success");
                      }
                    },
                      function (response) {
                        // wip(0);
                        if (response.error.error == "invalid_token") {
                          this.data.alert("Could Not Connect To Server", "danger");
                          this.data.logout();
                        } else {
                          this.data.logout();
                          this.data.alert("Could Not Connect To Server", "danger");
                        }
                      }
                    );
                } else {
                  this.reset();
                  this.data.loader = false;
                  this.data.alert(
                    "Offer Value is lesser than permissible value",
                    "warning"
                  );
                }
              }
            });
        } else {
          this.onlySellPrice = 0;
          $(".onlySellError").show();
        }
      });
  }

  limitBuy() {
    $(".tradeBtn").attr("disabled", true);
    this.data.alert("Loading...", "dark", 30000);
    var inputObj = {};
    inputObj[
      "selling_asset_code"
    ] = this.data.selectedSellingAssetText.toUpperCase(); // change by sanu
    inputObj[
      "buying_asset_code"
    ] = this.data.selectedBuyingAssetText.toUpperCase(); //
    inputObj["userId"] = localStorage.getItem("user_id");
    inputObj["price"] = this.limitPrice;
    inputObj["txn_type"] = "1";
    var jsonString = JSON.stringify(inputObj);
    if (this.limitPrice * this.limitAmount > this.valLimit) {
      this.limitbuyOfferpriceApi = this.http
        .post<any>(
          this.data.WEBSERVICE + "/userTrade/OfferPriceCheck",
          jsonString,
          {
            headers: {
              "Content-Type": "application/json"
            }
          }
        )
        .subscribe(response => {
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "warning");
            $(".tradeBtn").attr("disabled", true);
          } else {
            //START
            if (this.limitAmount != undefined && this.limitPrice != undefined) {
              var inputObj = {};
              inputObj["userId"] = localStorage.getItem("user_id");
              inputObj[
                "selling_asset_code"
              ] = this.data.selectedSellingAssetText.toUpperCase();
              inputObj[
                "buying_asset_code"
              ] = this.data.selectedBuyingAssetText.toUpperCase();
              inputObj["amount"] = this.limitAmount;
              inputObj["price"] = this.limitPrice;
              inputObj["offerType"] = 'L';
              inputObj["txn_type"] = "1";
              var jsonString = JSON.stringify(inputObj);
              this.limitbuytradeOffer = this.http
                .post<any>(
                  this.data.WEBSERVICE + "/userTrade/TradeCreateOffer",
                  jsonString,
                  {
                    headers: {
                      "Content-Type": "application/json",
                      authorization:
                        "BEARER " + localStorage.getItem("access_token")
                    }
                  }
                )
                .subscribe(response => {
                  this.data.loader = false;
                  var result = response;
                  if (result.error.error_data != "0") {
                    this.data.alert(result.error.error_msg, "danger");
                    $(".tradeBtn").removeAttr("disabled");
                    $(".form-control").val("");
                    $("#totalValueTrade").val("");
                    $(".tradeBtn").attr("disabled", true);
                    //   if (result.error.error_data == 1)
                    //     this.data.alert(result.error.error_msg, "danger");
                    //   else $("#warn").click();
                    //   $(".tradeBtn").removeAttr("disabled");
                    //   $(".form-control").val("");
                    //   $("#totalValueTrade").val("");
                    //   $(".tradeBtn").attr("disabled", true);
                    // } else {
                    //   $(".form-control").val("");
                    //   this.data.alert(result.error.error_msg, "success");
                    //   this.limitPrice = 0;
                    //   this.limitAmount = 0;
                    //   this.reset();
                    //   $("#trade").click();
                  }
                  else if (result.error.error_data == "0") {
                    $(".form-control").val("");
                    this.data.alert(result.error.error_msg, "success");
                    this.limitPrice = 0;
                    this.limitAmount = 0;
                    this.reset();
                    $("#trade").click();

                  }
                },
                  function (response) {
                    // wip(0);
                    //    alert('mnbmnbm');
                    if (response.error.error == "invalid_token") {
                      this.data.alert("Could Not Connect To Server", "danger");
                      this.data.logout();
                    } else {
                      this.data.logout();
                      this.data.alert("Could Not Connect To Server", "danger");
                    }
                  }
                );
            } else {
              //$("#warn").click();
              $(".tradeBtn").removeAttr("disabled");
              $(".form-control").val("");
              this.data.alert(
                "Please provide proper buying details",
                "warning"
              );
            }
            //End
          }
          this.limitAmount = this.limitPrice = this.limitValue = null;
        });
    } else {
      this.limitAmount = this.limitPrice = this.limitValue = null;
      this.data.loader = false;
      this.data.alert("Your offer is too small", "warning");
    }
  }

  limitSell() {
    $(".tradeBtn").attr("disabled", true);
    this.data.alert("Loading...", "dark", 30000);
    if (this.limitPrice != undefined && this.limitAmount != undefined) {
      var inputObj = {};
      inputObj[
        "selling_asset_code"
      ] = this.data.selectedBuyingAssetText.toUpperCase();
      inputObj[
        "buying_asset_code"
      ] = this.data.selectedSellingAssetText.toUpperCase(); //change by sanu
      inputObj["userId"] = localStorage.getItem("user_id");
      inputObj["price"] = this.limitPrice;
      inputObj["offerType"] = 'L';
      inputObj["txn_type"] = "2";
      var jsonString = JSON.stringify(inputObj);
      if (this.limitPrice * this.limitAmount > this.valLimit) {
        this.limitsellOfferpricecheck = this.http
          .post<any>(
            this.data.WEBSERVICE + "/userTrade/OfferPriceCheck",
            jsonString,
            {
              headers: {
                "Content-Type": "application/json"
              }
            }
          )
          .subscribe(response => {
            var result = response;
            if (result.error.error_data != "0") {
              this.data.alert(result.error.error_msg, "danger");
              $(".tradeBtn").attr("disabled", true);
            } else {
              var inputObj = {};
              inputObj["userId"] = localStorage.getItem("user_id");
              inputObj[
                "selling_asset_code"
              ] = this.data.selectedBuyingAssetText.toUpperCase();
              inputObj[
                "buying_asset_code"
              ] = this.data.selectedSellingAssetText.toUpperCase();
              inputObj["amount"] = this.limitAmount;
              inputObj["price"] = this.limitPrice;
              inputObj["offerType"] = 'L';
              inputObj["txn_type"] = "2";
              var jsonString = JSON.stringify(inputObj);
              this.limitselltradecreateoffer = this.http
                .post<any>(
                  this.data.WEBSERVICE + "/userTrade/TradeCreateOffer",
                  jsonString,
                  {
                    headers: {
                      "Content-Type": "application/json",
                      authorization:
                        "BEARER " + localStorage.getItem("access_token")
                    }
                  }
                )
                .subscribe(response => {
                  this.data.loader = false;
                  var result = response;
                  if (result.error.error_data != "0") {

                    this.data.alert(result.error.error_msg, "danger");
                    $(".tradeBtn").removeAttr("disabled");
                    $(".form-control").val("");
                    $("#totalValueTrade").val("");
                    $(".tradeBtn").attr("disabled", true);
                  }
                  else {
                    $(".form-control").val("");
                    this.data.alert(result.error.error_msg, "success");
                    this.limitAmount = 0;
                    this.limitPrice = 0;
                    this.reset();
                    $("#trade").click();
                  }
                },
                  function (response) {
                    if (response.error.error == "invalid_token") {
                      this.data.alert("Could Not Connect To Server", "danger");
                      this.data.logout();
                    } else {
                      this.data.logout();
                      this.data.alert("Could Not Connect To Server", "danger");
                    }
                  }
                );
            }
            this.limitAmount = this.limitPrice = this.limitValue = null;
          });
      } else {
        this.limitAmount = this.limitPrice = this.limitValue = null;
        this.data.loader = false;
        this.data.alert("Your offer is too small", "warning");
      }
    } else {
      $(".form-control").val("");
      $(".tradeBtn").removeAttr("disabled");
      this.data.alert("Please provide proper selling details", "warning");
    }
  }

  getUserTransaction() {
    var userTransObj = {};
    userTransObj["customerId"] = localStorage.getItem("user_id");
    var jsonString = JSON.stringify(userTransObj);
    //wip(1);
    this.getusetranApi = this.http
      .post<any>(
        this.data.WEBSERVICE + "/transaction/getUserBalance",
        jsonString,
        {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        }
      )
      .subscribe(
        response => {
          var result = response;
          this.balencelist = result.userBalanceList;
          this.currencyBalance = this.balencelist;
          if (this.currencyBalance != null) {
            for (var i = 0; i < this.currencyBalance.length; i++) {
              if (this.currencyBalance[i].currencyCode == "TRY") {
                localStorage.setItem(
                  "usdbalance",
                  this.currencyBalance[i].closingBalance
                );
              }
            }
          }
          if (result.error.error_data != "0") {
            this.data.alert("Cannot fetch user balance", "danger");
          } else {
            this.selectedCryptoCurrency = localStorage.getItem(
              "selected_currency"
            );
            localStorage.getItem("selling_crypto_asset");
            localStorage.getItem("buying_crypto_asset");
            for (var i = 0; i <= this.balencelist.length - 1; i++) {
              if (
                this.balencelist[i].currencyCode ==
                localStorage.getItem("buying_crypto_asset").toUpperCase()
              ) {
                this.selelectedBuyingAssetBalance = this.balencelist[
                  i
                ].closingBalance.toFixed(4);
              }
              if (
                this.balencelist[i].currencyCode ==
                localStorage.getItem("selling_crypto_asset").toUpperCase()
              ) {
                this.selelectedSellingAssetBalance = this.balencelist[
                  i
                ].closingBalance.toFixed(4);
              }
            }
          }
        },
        function (reason) {
          // wip(0);
          if (reason.error.error == "invalid_token") {
            this.data.logout();
          } else {
            console.error(reason);
          }
        }
      );
  }

  sellStoploss() {
    $("#placeOrderForStopLossBtn").attr("disabled", true);
    $(".stopLossError").hide();
    this.data.alert("Loading...", "dark");
    if (
      this.stopLossPrice != undefined &&
      this.stopLossTriggerPrice != undefined &&
      this.stopLossQuantity != undefined
    ) {
      // wip(1);
      
      this.sellStoplossApi = this.http
        .get<any>(
          this.data.TRADESERVICE +
          "/getAmountSell/" +
          localStorage.getItem("buying_crypto_asset").toUpperCase() +
          localStorage.getItem("selling_crypto_asset").toUpperCase() +
          "/" +
          localStorage.getItem("selling_crypto_asset").toUpperCase() +
          localStorage.getItem("buying_crypto_asset").toUpperCase() +
          "/" +
         '0.00001'
        )
        .subscribe(data => {
          $("#placeOrderForStopLossBtn").attr("disabled", false);
          // wip(0);
          var result = data;
          if (result.code == "0") {
            this.marketOrderPrice = parseFloat(result.price);
            if (
              this.marketOrderPrice > this.stopLossTriggerPrice &&
              this.marketOrderPrice > this.stopLossPrice &&
              this.stopLossTriggerPrice > this.stopLossPrice
            ) {
              var inputObj = {};
              inputObj["buying_asset_code"] = localStorage
                .getItem("selling_crypto_asset")
                .toUpperCase();
              inputObj["userId"] = localStorage.getItem("user_id");
              inputObj["selling_asset_code"] = localStorage
                .getItem("buying_crypto_asset")
                .toUpperCase();
              inputObj["quantity"] = this.stopLossQuantity;
              inputObj["stop_loss_price"] = this.stopLossPrice;
              inputObj["trigger_price"] = this.stopLossTriggerPrice;
              inputObj["txn_type"] = "2";
              var jsonString = JSON.stringify(inputObj);
              // wip(1);
              this.stplossbysellApi = this.http
                .post<any>(
                  this.data.WEBSERVICE + "/userTrade/StopLossBuySellTrade",
                  jsonString,
                  {
                    headers: {
                      "Content-Type": "application/json"
                    }
                  }
                )
                .subscribe(data => {
                  // wip(0);
                  this.data.loader = false;
                  var result = data;
                  if (result.error.error_data != "0") {
                    // if (result.error.error_data == 1)
                    this.data.alert(result.error.error_msg, "danger");
                    //  $("#warn").click();
                  } else {
                    this.data.alert(result.error.error_msg, "success");
                    $("#trade").click();
                    this.reset();
                  }
                });
            } else {
              this.stopLossError =
                "*Market order price should be greater than trigger price & trigger price should be greater than stop loss price";
              $(".stopLossError").html(this.stopLossError);
              $(".stopLossError").show();
              this.data.loader = false;
            }
          } else {
            // this.stopLossError = "*Orderbook depth reached, price not found";
            this.stopLossError = "*Error ";
            $(".stopLossError").html(this.stopLossError);
            $(".stopLossError").show();
            this.data.loader = false;
          }
        });
    } else {
      this.data.alert("Please Provide Proper Details", "error");
    }
  }

  buyStopLoss() {
    $("#buyForStopLossBtn").attr("disabled", true);
    $(".stopLossError").hide();
    this.data.alert("Loading...", "dark");
    if (
      this.stopLossPrice != undefined &&
      this.stopLossTriggerPrice != undefined &&
      this.stopLossQuantity != undefined
    ) {
      // wip(1);
      this.buystoplossApi = this.http
        .get<any>(
          this.data.TRADESERVICE +
          "/getAmountBuy/" +
          localStorage.getItem("selling_crypto_asset").toUpperCase() +
          localStorage.getItem("buying_crypto_asset").toUpperCase() +
          "/" +
          localStorage.getItem("buying_crypto_asset").toUpperCase() +
          localStorage.getItem("selling_crypto_asset").toUpperCase() +
          "/" +
         '0.00001'
        )
        .subscribe(data => {
          $("#buyForStopLossBtn").attr("disabled", false);
          // wip(0);
          var result = data;
          if (result.code == "0") {
            this.marketOrderPrice = parseFloat(result.price);

            console.log(
              this.marketOrderPrice,
              this.stopLossTriggerPrice,
              this.stopLossPrice,
              this.stopLossQuantity
            );

            if (
              this.marketOrderPrice < this.stopLossTriggerPrice &&
              this.marketOrderPrice < this.stopLossPrice &&
              this.stopLossTriggerPrice < this.stopLossPrice
            ) {
              var inputObj = {};
              inputObj["buying_asset_code"] = localStorage
                .getItem("buying_crypto_asset")
                .toUpperCase();
              inputObj["userId"] = localStorage.getItem("user_id");
              inputObj["selling_asset_code"] = localStorage
                .getItem("selling_crypto_asset")
                .toUpperCase();
              inputObj["quantity"] = this.stopLossQuantity;
              inputObj["stop_loss_price"] = this.stopLossPrice;
              inputObj["trigger_price"] = this.stopLossTriggerPrice;
              inputObj["txn_type"] = "1";
              var jsonString = JSON.stringify(inputObj);
              // wip(1);
              this.buystoplossApi = this.http
                .post<any>(
                  this.data.WEBSERVICE + "/userTrade/StopLossBuySellTrade",
                  jsonString,
                  {
                    headers: {
                      "Content-Type": "application/json"
                    }
                  }
                )
                .subscribe(data => {
                  this.data.loader = false;
                  // wip(0);
                  var result = data;
                  if (result.error.error_data != "0") {
                    //  if (result.error.error_data == 1)
                    this.data.alert(result.error.error_msg, "danger");
                    //  $("#warn").click();
                  } else {
                    this.data.alert(result.error.error_msg, "success");
                    $("#trade").click();
                    this.reset();
                    // wip(0);
                    //location.reload();
                  }
                  this.stopLossPrice = this.stopLossTriggerPrice = this.stopLossQuantity = null;
                });
            } else {
              this.stopLossError =
                "*Market order price should be less than trigger price & trigger price should be less than stop loss price";
              $(".stopLossError").html(this.stopLossError);
              $(".stopLossError").show();
              this.data.loader = false;
            }
          } else {
           // this.stopLossError = "*Orderbook depth reached, price not found";
            this.stopLossError = "* Error";
            $(".stopLossError").html(this.stopLossError);
            $(".stopLossError").show();
            this.data.loader = false;
          }
        });
    } else {
      this.data.alert("Please Provide Proper Details", "warning");
    }
  }

  // warnKyc(content) {
  //   this.modalService.open(content, {
  //     centered: true
  //   });
  // }

  nonNg(event) {
    var val = event.target.value;
    if (val < 0) this.data.alert("Price cannot be negative", "warning");
  }

  send(content, val) {
    this.mywallet.getCurrencyForSend(content, val, "", "");
  }

  validateLimit() {
    var lv: number = 0.000001;
    return (
      0.0001 >= this.limitAmount ||
      0.00000001 >= this.limitPrice /* || lv >= this.limitValue*/
    );
  }
  ngOnDestroy() {
    if (this.getamntsell != undefined) {
      this.getamntsell.unsubscribe();
    }
    if (this.sellStoplossApi != undefined) {
      this.sellStoplossApi.unsubscribe();
    }
    if (this.buystoplossApi != undefined) {
      this.buystoplossApi.unsubscribe();
    }
    if (this.stplossbysellApi != undefined) {
      this.stplossbysellApi.unsubscribe();
    }
    if (this.OfferpricecheckApi != undefined) {
      this.OfferpricecheckApi.unsubscribe();
    }
    if (this.tradecreateApi != undefined) {
      this.tradecreateApi.unsubscribe();
    }
    if (this.marketsellApi != undefined) {
      this.marketsellApi.unsubscribe();
    }
    if (this.limitbuytradeOffer != undefined) {
      this.limitbuytradeOffer.unsubscribe();
    }
    if (this.marketsellOfferpricecheck != undefined) {
      this.marketsellOfferpricecheck.unsubscribe();
    }
    if (this.limitselltradecreateoffer != undefined) {
      this.limitselltradecreateoffer.unsubscribe();
    }
    if (this.limitbuytradeOffer != undefined) {
      this.limitbuytradeOffer.unsubscribe();
    }
    if (this.limitbuyOfferpriceApi != undefined) {
      this.limitbuyOfferpriceApi.unsubscribe();
    }
    if (this.limitsellOfferpricecheck != undefined) {
      this.limitsellOfferpricecheck.unsubscribe();
    }
    if (this.limitselltradecreateoffer != undefined) {
      this.limitselltradecreateoffer.unsubscribe();
    }
    if (this.currencyapi != undefined) {
      this.currencyapi.unsubscribe();
    }
    if (this.amountby != undefined) {
      this.amountby.unsubscribe();
    }
    if (this.getmntbuy != undefined) {
      this.getmntbuy.unsubscribe();
    }

  }
}